//
//  SeparateChainingHashST.swift
//  Algorithms
//
//  Created by Mingming Wang on 16/08/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//
class ChainedKeyValueNode<K, V> {
  var key: K
  var value: V
  var next: ChainedKeyValueNode<K, V>?
  init(key: K, value: V, next: ChainedKeyValueNode<K, V>? = nil) {
    self.key = key
    self.value = value
    self.next = next
  }
}

class SeparateChainingHashST<K: Hashable, V>: SymbolTable<K, V> {
  private var chainedKeyValueNodes: [ChainedKeyValueNode<K, V>?]

  init(size: Int) {
    chainedKeyValueNodes = [ChainedKeyValueNode<K, V>?](repeating: nil, count: size)
  }

  override func put(key: K, value: V) {
    let index = hash(key: key)
    var current = chainedKeyValueNodes[index]
    while current != nil {
      if current!.key == key {
        current!.value = value
        return
      }
      current = current!.next
    }

    chainedKeyValueNodes[index] = ChainedKeyValueNode(key: key, value: value, next: chainedKeyValueNodes[index])
  }

  override func get(key: K) -> V? {
    let index = hash(key: key)
    var current = chainedKeyValueNodes[index]
    while (current != nil) {
      if current!.key == key {
        return current?.value
      }
      current = current!.next
    }
    return nil
  }

  private func hash(key: K) -> Int {
    return key.hashValue % size()
  }

}
