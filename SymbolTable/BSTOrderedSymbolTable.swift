//
//  BSTOrderedSymbolTable.swift
//  Algorithms
//
//  Created by Mingming Wang on 07/08/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//
class Node<Key: Comparable, Value> {
  var key: Key
  var value: Value
  var size: Int // number of nodes in tree that rooted at current node
  var left: Node?
  var right: Node?
  init(key: Key, value: Value, size: Int) {
    self.key = key
    self.value = value
    self.size = size
  }
}

class BSTOrderedSymbolTable<Key: Comparable, Value>: OrderedSymbolTable<Key, Value> {
  var bst: Node<Key, Value>? = nil
  // Number of compares is equal to 1 + depth of node.
  override func get(key: Key) -> Value? {
    var node = bst
    while node != nil {
      if key > node!.key  {
        node = node!.right
      } else if key < node!.key {
        node = node!.left
      } else {
        return node!.value
      }
    }
    return nil
  }
  // Number of compares is equal to 1 + depth of node.
  override func put(key: Key, value: Value) {
    bst = put(root: bst, key: key, value: value)
  }

  private func put(root: Node<Key, Value>?, key: Key, value: Value) -> Node<Key, Value> {
    guard let root = root else {
      return Node(key: key, value: value, size: 1)
    }
    if key < root.key {
      root.left = put(root: root.left, key: key, value: value)
    } else if key > root.key {
      root.right = put(root: root.right, key: key, value: value)
    } else {
      root.value = value
    }
    root.size = 1 + size(root: root.left) + size(root: root.right)
    return root
  }

  override func size() -> Int {
    return size(root: bst)
  }

  private func size(root: Node<Key, Value>?) -> Int {
    if root == nil { return 0 }
    return root!.size
  }

  override func rank(key: Key) -> Int {
    return rank(key: key, root: bst)
  }

  private func rank(key: Key, root: Node<Key, Value>?) -> Int{
    guard let root = root else { return 0 }
    if key == root.key {
      return size(root: root.left)
    } else if key < root.key {
      return rank(key: key, root: root.left)
    } else {
      return 1 + size(root: root.left) + rank(key: key, root: root.right)
    }
  }

  override func makeIterator() -> AnyIterator<Key> {
    let queue = LinkedListQueue<Key>()
    inorder(root: bst, queue: queue)
    return queue.makeIterator()
  }

  private func inorder(root: Node<Key, Value>?, queue: Queue<Key>) {
    guard let root = root else { return }
    inorder(root: root.left, queue: queue)
    queue.enqueue(root.key)
    inorder(root: root.right, queue: queue)
  }

  override func deleteMin() {
    bst = deleteMin(node: bst)
  }

  private func deleteMin(node: Node<Key, Value>?) -> Node<Key, Value>? {
    guard let node = node else { return nil }
    if node.left == nil {
      return node.right
    } else {
      node.left = deleteMin(node: node.left)
      node.size = 1 + size(root: node.left) + size(root: node.right)
      return node
    }
  }

  override func delete(key: Key) {
    bst = delete(key: key, node: bst)
  }

  private func delete(key: Key, node: Node<Key, Value>?) -> Node<Key, Value>? {
    guard var node = node else { return nil }
    if key == node.key {
      if node.left == nil {
        return node.right
      }
      if node.right == nil {
        return node.left
      }
      let t = node
      node = minNode(node: t.right)!
      node.right = deleteMin(node: t.right)
      node.left = t.left
    } else if key > node.key {
      node.right = delete(key: key, node: node.right)
    } else {
      node.left = delete(key: key, node: node.left)
    }
    node.size = 1 + size(root: node.right) + size(root: node.left)
    return node
  }

  override func max() -> Key? {
    var m = bst
    while m?.right != nil {
      m = m?.right
    }
    return m?.key
  }

  override func min() -> Key? {
    var m = bst
    while m?.left != nil {
      m = m?.left
    }
    return m?.key
  }

  private func minNode(node: Node<Key, Value>?) -> Node<Key, Value>?{
    guard let node = node else { return nil }
    if node.left != nil {
      return minNode(node: node.left)
    } else {
      return node
    }
  }

  override func floor(key: Key) -> Key? {
    return floor(node: bst, key: key)
  }

  private func floor(node: Node<Key, Value>?, key: Key) -> Key? {
    guard let root = node else { return nil }
    var fl: Key? = nil
    if key > root.key {
      let floorRight: Key? = floor(node: root.right, key: key)
      fl = floorRight != nil ? floorRight! : root.key
    } else if key < root.key {
      fl = floor(node: root.left, key: key)
    } else {
      fl = root.key
    }
    return fl
  }
}
