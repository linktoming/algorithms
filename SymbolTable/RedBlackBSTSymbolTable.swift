//
//  RedBlackBST.swift
//  Algorithms
//
//  Created by Mingming Wang on 13/08/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

typealias RedBlackBSTSymbolTable<Key:Comparable, Value> = RedBlackBST<Key, Value>

enum RBLinkColor {
  case Red
  case Black
}

class RBNode<Key: Comparable, Value> {
  var key: Key
  var value: Value
  var color: RBLinkColor // color of parent link
  var left: RBNode<Key, Value>?
  var right: RBNode<Key, Value>?
  init(key: Key, value: Value, color: RBLinkColor) {
    self.key = key
    self.value = value
    self.color = color
  }
}

class RedBlackBST<Key: Comparable, Value>: OrderedSymbolTable<Key, Value> {
  var root: RBNode<Key, Value>?

  override func get(key: Key) -> Value? {
    var current: RBNode<Key, Value>! = root
    while (current != nil) {
      if current.key == key {
        return current.value
      } else if current.key > key {
        current = current.left
      } else {
        current = current.right
      }
    }
    return nil
  }

  private func isRed(node: RBNode<Key, Value>?) -> Bool {
    guard let node = node else {
      return false
    }
    return node.color == .Red
  }

  private func rotateLeft(node: RBNode<Key, Value>) -> RBNode<Key, Value> {
    assert(isRed(node: node.right))
    let newRoot = node.right
    node.right = newRoot?.left
    newRoot?.left = node
    newRoot?.color = node.color
    node.color = .Red
    return newRoot!
  }

  private func rotateRight(node: RBNode<Key, Value>) -> RBNode<Key, Value> {
    assert(isRed(node: node.left))
    let newRoot = node.left
    node.left = newRoot?.right
    newRoot?.right = node
    newRoot?.color = node.color
    node.color = .Red
    return newRoot!
  }

  private func flipColors(node: RBNode<Key, Value>) {
    assert(!isRed(node: node))
    assert(isRed(node: node.left))
    assert(isRed(node: node.right))

    // if node is root, then the color doesn't mean anything since it doesn't have parent link
    node.color = .Red
    node.left?.color = .Black
    node.right?.color = .Black
  }

  override func put(key: Key, value: Value) {
    root = put(node: root?.right, key: key, value: value)
  }

  private func put(node: RBNode<Key, Value>?, key: Key, value: Value) -> RBNode<Key, Value> {
    guard var node = node else { return RBNode(key: key, value: value, color: .Red) }

    if node.key == key {
      node.value = value
    } else if node.key > key {
      node.left = put(node: node.left, key: key, value: value)
    } else {
      node.right = put(node: node.right, key: key, value: value)
    }

    if isRed(node: node.right) && !isRed(node: node.left) {
      node = rotateLeft(node: node)
    }

    if isRed(node: node.left) && isRed(node: node.left?.left) {
      node = rotateRight(node: node)
    }

    if isRed(node: node.left) && isRed(node: node.right) {
      flipColors(node: node)
    }

    return node
  }
}
