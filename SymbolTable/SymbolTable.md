Symbol table is a data structure with key-value pair abstraction.
- `insert` a value with specified key
- given a key, `search` for the corresponding value

## Basic API
Its basic API uses a associative array abstraction. For example, `arrayA[aKey] = aValue`, the `aKey` here is not necessarily number but can be strings or 
other type. Refer to `SymbolTable.swift` for its API. 

## Conventions
There are certain convention to make symbol table implementation easier. 
- values are not null
- method `get` returns null if there is no such key
- method `put` overwrites old value with new value

For example, we can implement `contains` by checking whether `get(key)` is equal to null. We can also implement lazy version of 
`delete` that `put` null for the value of the to be deleted key.

The NSDictionary and NSMutableDictionary in Objective-C are suspected to adopt this convention based on their behaviour.

## Values and Keys
Values of a symbol table can be any generic type.

The keys normally will support one of the following interface.
- keys are comparable. 
- keys can be tested by equality.
- keys can have unique hash code. (`hashCode()` in Java)

Best practices are to use immutable types for symbol table keys.

### Common pattern for implementing `equals` (in Java)
- class has to be final. It's unsafe to use `equals` with inheritance.
- optimize for check equality again oneself. `if other == self { return true }`
- check for null. `if other == null { return false }`
- check for same type. If not, return false 
- then, finally check for all significant fields are the same
  - if field is primitive type, check value equality
  - if field is object, use the object itself's `equals` method
  - if field is collection type, check equality for each items in the collection
- no need to compare calculated fields
- compare fields most likely to be different first
- make `compareTo`'s logic consistent with `equals`

### Common pattern for implementing immutability
Data Type: set of values and operations on these values
Immutable data type: can't change data type value once created.

*Classes should be immutable unless there's a very good reason to 
make them mutable.... If a class cannot be made immutable, you 
should still limit its mutability as much as possible.*
#### Implementation
- mark class final so that instance methods cannot be overrided 
- mark all instance variable private and final
- defensive copy of mutable instance variable passed in in initializer
  or other methods
- instance methods don't change instance variables

#### Advantage 
- simplifies debugging
- safer in presence of hostile code
- simplifies concurrent programming
- safe to use as key in priority queue or symble table

#### Disadvantage
Must create new object for each data type value.

## Elementary Implementations of Symbol table
- Sequential Search in unordered linked list: maintain an unordered linked list of key-value pairs
  - Search: scan through all keys until find a match
  - Insert: scan through all keys until find a match; if no match, add to the front
- Binary search in an ordered array: Maintain an ordered array of key-value pairs.
  - Search: binary search using rank helper
  - Insert: insertion sort, have to put the item into place by shifting larger items to the left

## Binary Search Tree (BST)
A binary search tree (BST) is a binary tree with symmetric order.
- binary tree: empty or node with links to left and right binary trees
- symmetric order: each node has a key, and every node’s key is:
  - larger than all keys in its left subtree. 
  - smaller than all keys in its right subtree.
- programmingly, a BST is a reference to its root node.


### BST Implementation of Ordered Symbol Table
- search: if less, go left; if greater, go right; if euqal, search hit;
- insert/put: if less, go left; if greater, go right; if null, insert; if search hit, reset value.
- get: return value corresponding to given key, or null if no such key.
- delete: delete the node with given key

#### Deletion in BST
- lazy approach: set the value of the to be delete node to be null and keep it in the tree to guide future search
  - ~ 2 ln N' per insert, search and delete for keys in random order where N' is the number of keys ever inserted
  - If too mange keys are deleted, we may have tombstone (memory) overload.
- delete the minimium key: 
  - find the minimium key by going left until find a null key
  - replace that to be deleted node by its right link
  - update subtree counts
- Hibbard deletion: to delete a node with key k, search for node t containing key k
  - if node t has no children, delete t by setting parent link to nil
  - if node t has one child, replace t with its child
  - if node t have two children, find the smallest node in t's right sub tree, delete it and put it into t's place.
  - the bst using Hibbard deletion is not symmetric and if the keys are not random, it may take square(N) per operation.
 
#### BST Tree Shapes
- many tree shapes correspond to the same set of keys
- number of compares for search/insert is equal to 1 + depth of node
- the tree shape will depends on the sequence of user input of which we have no control over. We canot 
  shuffle it as what we did for quick sort since it's from user input one by one not necessarily one whole set given at once.

If N distinct keys are inserted into a BST in random order, the expected number of compares for a search/insert is ~ 2 ln N.
(Correspondence between BST and quick sort is 1-1 if array has no duplicate keys.)
But worst case the height of BST is N (expontentialy small chance when keys are inserted in random order).

One longstanding open problem is to find simple and efficient delete for BSTs.

## Summary
### Implementation Summary
|Symbol Table Implementation|Worst Case after N Inserts||Average Case after N random inserts||Ordered Iteration?|Key Interface|
|---|---|---|---|---|---|---|
||Search|Insert|Search Hit|Insert|||
|Sequential Search (unordered list)|N|N|N/2|N|no|equals()|
|Binary Search (ordered array)|lgN|N|lgN|N/2|yes|compareTo()|
![ST Implementations Summary](images/ST_Implementations_Summary.png)
### Ordered Symbol Table Operations Summary
![Ordered Operations Summary](images/Ordered_Operations_Summary.png)

