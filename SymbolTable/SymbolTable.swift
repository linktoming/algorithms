//
//  SymbolTable.swift
//  Algorithms
//
//  Created by Mingming Wang on 27/07/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

class SymbolTable<Key, Value>: Sequence {
  func put(key: Key, value: Value) { methodUnImplementedError(#function) }
  func get(key: Key) -> Value? { methodUnImplementedError(#function) }
  func delete(key: Key) { methodUnImplementedError(#function) }
  func contains(key: Key) -> Bool {
    return get(key: key) != nil
  }
  func isEmpty() -> Bool { methodUnImplementedError(#function) }
  func makeIterator() -> AnyIterator<Key> { methodUnImplementedError(#function) }
  func size() -> Int { methodUnImplementedError(#function) }
}

class OrderedSymbolTable<Key: Comparable, Value>: SymbolTable<Key, Value> {
  // the smallest key
  func min() -> Key? {
    methodUnImplementedError(#function)
  }
  func deleteMin() {
    methodUnImplementedError(#function)
  }
  // the largest key
  func max() -> Key? {
    methodUnImplementedError(#function)
  }
  func deleteMax() {
    methodUnImplementedError(#function)
  }
  // count of keys < key
  func rank(key: Key) -> Int {
    methodUnImplementedError(#function)
  }
  // largest key that less than or equal to key
  func floor(key: Key) -> Key? {
    methodUnImplementedError(#function)
  }
  // smallest key that greater or equal to key
  func ceiling(key: Key) -> Key {
    methodUnImplementedError(#function)
  }
  // key of rank
  func select(rank: Int) -> Key {
    methodUnImplementedError(#function)
  }
  // the item count between low and high both inclusive
  func size(low: Key, high: Key) -> Int {
    if contains(key: high) {
      return rank(key: high) - rank(key: low) + 1
    } else {
      return rank(key: high) - rank(key: low)
    }
  }
  // keys in [low...high] in sorted order
  func keys(low: Key, high: Key) -> [Key]{
    methodUnImplementedError(#function)
  }
}
