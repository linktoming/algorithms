Common balanced search tress are 2-3 trees, left-leaning red-black BSTs and B-trees.
## 2-3 trees
- Allow 1 or 2 keys per node.
  - 2-node: one key, two children. 
  - 3-node: two keys, three children.
- Symmetric order: Inorder traversal yields keys in ascending order.
- Perfect balance: Every path from root to null link has same length.

Sample

![2-3 tree](images/2-3Tree.png)
### Search
- Compare search key against keys in node. 
- Find interval containing search key. 
- Follow associated link (recursively).
### Insertion into a 3-node at bottom
- Add new key to 3-node to create temporary 4-node.
- Move middle key in 4-node into parent.
- Repeat up the tree, as necessary.
- If you reach the root and it's a 4-node, split it into three 2-nodes.

### Properties of 2-3 Trees
- Splitting a 4-node is a local transformation: constant number of operations.
- Maintains symmetric order and perfect balance: Each transformation maintains symmetric order and perfect balance.
- Perfect balance: Every path from root to null link has same length.
- Tree height
  - Worst case: lg N. [all 2-nodes]
  - Best case: log3 N ≈ .631 lg N. [all 3-nodes] 
  - Between 12 and 20 for a million nodes. 
  - Between 18 and 30 for a billion nodes.
- Guaranteed logarithmic performance for search and insert.

### 2-3 Tree Implementation
Direct implementation is complicated, because:
- Maintaining multiple node types is cumbersome. 
- Need multiple compares to move down tree. 
- Need to move back up the tree to split 4-nodes. 
- Large number of cases for splitting.

Bottom line: Could do it, but there's a better way.

## left-leaning red-black BSTs (LLRB)
LLRB represents a 2-3 tree using BST by using "internal" left-leaning links as "glue" for 3-nodes.
Large key as the root.
![LLRB](images/LLRB.png)

Put it in another way, LLRB is a BST such that:
- No node has two red links connected to it.
- Every path from root to null link has the same number of black links. (perfect black balance)
- Red links lean left.

1–1 correspondence between 2–3 and LLRB

![LLRB](images/LLRB2.png)
### Elementary red-black BST operations

#### Search in LLRB
Search in LLRB is the same as in common BSTs (ignoring color) but runs faster due to better balance. 
Most other ops (e.g., floor, iteration, selection) are also identical.

#### Left Rotation
Orient a (temporarily) right-leaning red link to lean left, which maintains symmetric order and perfect black balance.
![left rotation](images/left_rotation.png)

#### Right Rotation
Orient a left-leaning red link to (temporarily) lean right. 
![right rotation](images/right_rotation.png)

#### Flip Colors
Recolor to split a (temporary) 4-node.
![Flip Colors](images/flip_color.png)

#### Insertion
Basic strategy: Maintain 1-1 correspondence with 2-3 trees by applying elementary red-black BST operations.
![LLRB Warm up 1](images/llrb_warmup1.png) 
![LLRB Warm up 2](images/llrb_warmup2.png)
![LLRB Case 1](images/llrb_case1.png) 
![LLRB Case 2](images/llrb_case2.png)

Same code for all cases.
- Right child red, left child black: rotate left. 
- Left child, left-left grandchild red: rotate right. 
- Both children red: flip colors.

## B-trees