//
//  FrequencyCounter.swift
//  Algorithms
//
//  Created by Mingming Wang on 27/07/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import Foundation
struct FrequencyCounter {
  var symbolTable: SymbolTable<String, Int>
  init(st: SymbolTable<String, Int>) {
    self.symbolTable = st
  }

  func add(_ word: String) {
    if symbolTable.contains(word) {
      symbolTable.put(key: word, value: symbolTable.get(key: word)! + 1)
    } else {
      symbolTable.put(key: word, value: 1)
    }
  }

  func printWordWithFrequency() {
    for key in symbolTable {
      print("key: \(key)  value: \(symbolTable.get(key: key))")
    }
  }

  func wordWithMaxFrequency() -> (String, Int) {
    var maxWord = ""
    var maxCount = 0
    symbolTable.put(key: maxWord, value: maxCount)
    for key in symbolTable {
      if (symbolTable.get(key: key)! > maxCount) {
        maxWord = key
        maxCount = symbolTable.get(key: key)!
      }
    }

    return (maxWord, maxCount)
  }
}
