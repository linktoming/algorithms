//
//  BinarySearchSymbolTable.swift
//  Algorithms
//
//  Created by Mingming Wang on 06/08/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

class BinarySearchSymbolTable<Key: Comparable, Value>: SymbolTable<Key, Value> {
  var orderedArray = [(Key, Value)]()

  override func put(key: Key, value: Value) {
    if contains(key: key) {
      orderedArray[rank(key: key)].1 = value
    } else {
      orderedArray.append((key, value))
      if orderedArray.count == 1 { return }
      for i in orderedArray.count - 2 ... 0 {
        if less(first: orderedArray[i + 1].0, second: orderedArray[i].0) {
          swap(&orderedArray, first: i + 1, second: i)
        } else {
          break
        }
      }
    }
  }
  
  override func get(key: Key) -> Value? {
    let r = rank(key: key)
    if r < orderedArray.count {
      if orderedArray[r].0 == key {
        return orderedArray[r].1
      } else {
        return nil
      }
    } else {
      return nil
    }
  }

  override func delete(key: Key) {
    if contains(key: key) {
      orderedArray.remove(at: rank(key: key))
    }
  }

  override func isEmpty() -> Bool {
    return orderedArray.count == 0
  }

  override func makeIterator() -> AnyIterator<Key> {
    var current = 0
    return AnyIterator {
      if current > self.orderedArray.count - 1 {
        return nil
      }
      current = current + 1
      return self.orderedArray[current - 1].0
    }
  }

  func rank(key: Key) -> Int {
    var low = 0, high = orderedArray.count - 1
    while (low <= high) {
      let mid = low + (high - low)/2
      if orderedArray[mid].0 == key {
        return mid
      } else if orderedArray[mid].0 > key {
        high = mid - 1
      } else {
        low = mid + 1
      }
    }
    return low
  }
}
