//
//  LinearProbingHashST.swift
//  Algorithms
//
//  Created by Mingming Wang on 17/08/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

class LinearProbingHashST<K: Hashable, V>: SymbolTable<K, V> {
  var keys: [K?]
  var values: [V?]
  let size: Int
  var itemCount = 0
  // table size is recommended to to 2*N
  init(size: Int) {
    keys = [K?](repeating: nil, count: size)
    values = [V?](repeating: nil, count: size)
    self.size = size
  }

  override func put(key: K, value: V) {
    guard itemCount < size else { fatalError("symbol table is full.") }
    var index = hash(key: key)
    while keys[index] != nil {
      if keys[index] == key {
        values[index] = value
        return
      }
      index = (index + 1) % size
    }
    keys[index] = key
    values[index] = value
    itemCount = itemCount + 1
  }

  override func get(key: K) -> V? {
    var index = hash(key: key)
    var increase = 0
    while keys[index] != nil {
      if keys[index] == key {
        return values[index]
      }
      index = (index + 1) % size
      increase = increase + 1
      if increase == size {
        break
      }
    }
    return nil
  }

  private func hash(key: K) -> Int {
    return key.hashValue % size
  }
}
