Bag is data structure that supports adding items to it and iterate 
through them. It's useful in context that the order of the iterating
is not important to the client.

It can be implemented using stack or queue implementation with iteration support 
but without the `pop` or `dequeue` function respectively.