//
//  Bag.swift
//  Algorithms
//
//  Created by Mingming Wang on 16/7/16.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import Foundation
protocol Bag: Sequence {
  associatedtype T
  func add(_ item: T)
  func size() -> Int
}
