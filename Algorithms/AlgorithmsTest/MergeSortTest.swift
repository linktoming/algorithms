//
//  MergeSortTest.swift
//  Algorithms
//
//  Created by Mingming Wang on 25/7/16.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import XCTest

class MergeSortTest: XCTestCase {

  func testMergeSort() {
    var array = randomThousandNumberArray()
    XCTAssertFalse(isSorted(array))
    MergeSort.sort(&array)
    XCTAssertTrue(isSorted(array))
  }

}
