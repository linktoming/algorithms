//
//  EvaluateTest.swift
//  Algorithms
//
//  Created by Mingming Wang on 16/7/16.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import XCTest

class EvaluateTest: XCTestCase {
    func testEvaluation() {
      let ev = Evaluate(expression: "( 1 + ( (2 + 3 ) * ( 4* 5 ) ) )")
      XCTAssertEqual(ev.evaluate(), 101)
    }
}
