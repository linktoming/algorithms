//
//  KnuthShuffleTest.swift
//  Algorithms
//
//  Created by Mingming Wang on 24/07/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import XCTest

class KnuthShuffleTest: XCTestCase {

  func testShuffling() {
    let array = thousandNumberArray()
    var arrayB = array
    var arrayC = array
    KnuthShuffle.shuffle(&arrayB)
    KnuthShuffle.shuffle(&arrayC)
    XCTAssertFalse(arrayB == arrayC)

  }

}
