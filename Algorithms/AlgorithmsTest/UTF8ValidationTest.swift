//
//  UTF8ValidationTest.swift
//  Algorithms
//
//  Created by Mingming Wang on 18/09/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import XCTest

class Solution {
  func validUtf8(_ data: [Int]) -> Bool {
    var i = 0
    var isValid = true
    while (i < data.count) {
      let bytes = getBytes(num: data[i])
      if let bytes = bytes{
        let valid = isValidNum(nums: data, idx: i, bytes: bytes)
        isValid = isValid && valid
        if isValid == false {
          return false
        }
      } else {
        return false
      }
      i += bytes!
    }
    return isValid
  }

  func getBytes(num: Int) -> Int? {
    var count: Int?
    if num & 0x80 == 0 {
      count = 1
    } else if num & 0xE0 == 0b11000000 {
      count = 2
    } else if num & 0xF0 == 0b11100000 {
      count = 3
    } else if num & 0xF8 == 0b11110000 {
      count = 4
    } else {
      count = nil
    }
    return count
  }

  func isValidNum(nums: [Int], idx: Int, bytes: Int) -> Bool {
    if bytes == 1 { return true }
    if idx + bytes > nums.count {
      return false
    }
    for i in idx+1..<idx + bytes {
      if nums[i] & 0x80 != 0b10000000 {
        return false
      }
    }

    return true
  }
}

class UTF8ValidationTest: XCTestCase {

    func testExample() {
      let s = Solution()
      XCTAssertTrue(s.validUtf8([240,162,138,147]))
    }
}
