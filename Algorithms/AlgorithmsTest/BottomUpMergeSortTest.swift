//
//  BottomUpMergeSortTest.swift
//  Algorithms
//
//  Created by Mingming Wang on 28/07/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import XCTest

class BottomUpMergeSortTest: XCTestCase {
  func testBottomUpMergeSort() {
    var array = randomThousandNumberArray()
    XCTAssertFalse(isSorted(array))
    BottomUpMergeSort.sort(&array)
    XCTAssertTrue(isSorted(array))
  }
}
