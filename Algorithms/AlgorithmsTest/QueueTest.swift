//
//  QueueTest.swift
//  Algorithms
//
//  Created by Mingming Wang on 9/7/16.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import XCTest
class QueueTest: XCTestCase {
  var queue: Queue<String>!
  override func setUp() {
    super.setUp()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  func testLinkedListQueue() {
    queue = LinkedListQueue<String>()
    queueTestHelper()
  }
  
  func testLinkedListQueueIterator() {
    queue = LinkedListQueue<String>()
    forInTester()
  }
  
  func testArrayQueue() {
    queue = ArrayQueue<String>()
    queueTestHelper()
  }
  
  func testArrayQueueIterator() {
    queue = ArrayQueue<String>()
    forInTester()
  }
  
  func queueTestHelper() {
    queue.enqueue("to")
    queue.enqueue("be")
    queue.enqueue("or")
    XCTAssertTrue(queue.dequeue() == "to")
    XCTAssertTrue(queue.dequeue() == "be")
    queue.enqueue("not")
    queue.enqueue("to")
    queue.enqueue("be")
    XCTAssertTrue(queue.dequeue() == "or")
    XCTAssertTrue(queue.dequeue() == "not")
    XCTAssertTrue(queue.dequeue() == "to")
    XCTAssertTrue(queue.dequeue() == "be")
  }
  
  func forInTester() {
    let words = ["to", "be", "or", "not", "to", "be"]
    for word in words {
      queue.enqueue(word)
    }
    var actual = [String]()
    for item in queue {
      actual.append(item)
    }
    XCTAssertEqual(actual, words)
  }
}
