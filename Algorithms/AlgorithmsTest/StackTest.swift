//
//  StackTest.swift
//  Algorithms
//
//  Created by Mingming Wang on 9/7/16.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import XCTest

class StackTest: XCTestCase {
  var stack: Stack<String>!
  override func setUp() {
    super.setUp()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  func testLinkedListStack() {
    stack = LinkedListStack<String>()
    stackTestHelper()
  }
  
  func testLinkedListStackIterator() {
    stack = LinkedListStack<String>()
    forInTest()
  }
  
  func testArrayStack() {
    stack = ArrayStack<String>()
    stackTestHelper()
  }
  
  func testArrayStackIterator() {
    stack = ArrayStack<String>()
    forInTest()
  }
  
  func stackTestHelper() {
    stack.push("to")
    stack.push("be")
    stack.push("or")
    stack.push("not")
    stack.push("to")
    XCTAssertTrue(stack.pop() == "to")
    stack.push("be")
    XCTAssertTrue(stack.pop() == "be")
    XCTAssertTrue(stack.pop() == "not")
    stack.push("that")
    XCTAssertTrue(stack.pop() == "that")
    XCTAssertTrue(stack.pop() == "or")
    XCTAssertTrue(stack.pop() == "be")
    XCTAssertTrue(stack.pop() == "to")
  }
  
  func forInTest() {
    let words = ["to", "be", "or", "not", "to", "be"]
    for word in words {
      stack.push(word)
    }
    var arrayResult = [String]()
    for str in stack {
      arrayResult.append(str)
    }
    XCTAssertEqual(arrayResult, words.reversed())
  }
}
