//
//  UnorderedArrayMaxPQTest.swift
//  Algorithms
//
//  Created by Mingming Wang on 03/08/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import XCTest

class UnorderedArrayMaxPQTest: XCTestCase {
  func testUnorderedArrayMaxPQ() {
    let maxPQ = UnorderedArrayMaxPQ<Int>()
    testHelper(maxPQ)
  }

  func testBinaryHeapPQ() {
    let maxPQ = BinaryHeapMaxPQ<Int>()
    testHelper(maxPQ)
  }

  func testHelper(_ pq: MaxPriorityQueue<Int>) {
    let array = randomNumericArray(upTo: 1000000)
    for item in array {
      pq.insert(item)
    }
    XCTAssertTrue(pq.deleteMax() == 999999)
    XCTAssertTrue(pq.deleteMax() == 999998)
  }
}
