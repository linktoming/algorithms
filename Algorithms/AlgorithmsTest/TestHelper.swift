//
//  TestHelper.swift
//  Algorithms
//
//  Created by Mingming Wang on 03/08/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

func thousandNumberArray() -> [Int] {
  return numericArray(upTo: 1000)
}

func numericArray(upTo: Int) -> [Int] {
  var array = [Int]()
  for i in 0 ..< upTo {
    array.append(i)
  }
  return array
}

func randomThousandNumberArray() -> [Int] {
  var sequence = thousandNumberArray()
  KnuthShuffle.shuffle(&sequence)
  return sequence
}

func randomNumericArray(upTo: Int) -> [Int] {
  var sequence = numericArray(upTo: upTo)
  KnuthShuffle.shuffle(&sequence)
  return sequence
}
