//
//  EvaluateTest.swift
//  Algorithms
//
//  Created by Mingming Wang on 16/7/16.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import XCTest

class CommonTest: XCTestCase {
  class Dummy: Sort {
    static func sort<T : Comparable>(_ array: inout [T]) { }
  }
  
  func testLess() {
    XCTAssertTrue(less(first: 10, second: 20))
    XCTAssertFalse(less(first: 10, second: 10))
    XCTAssertFalse(less(first: 10, second: 9))
  }
  
  func testswap() {
    var array = [1,2,3,4]
    swap(&array, first: 0, second: 3)
    XCTAssertEqual(array, [4,2,3,1])
  }
  
  func testIsSorted() {
    XCTAssertTrue(isSorted([1,1,2,4,5,5]))
    XCTAssertFalse(isSorted([1,1,2,4,5,5].reversed()))
    XCTAssertFalse(isSorted([1,1,2,1,5,5]))
  }

  func testNumberArray() {
    XCTAssertTrue(isSorted(thousandNumberArray()))
  }

  func testRandomArray() {
    XCTAssertFalse(isSorted(randomThousandNumberArray()))
  }
}
