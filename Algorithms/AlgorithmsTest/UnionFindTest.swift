//
//  AlgorithmsTest.swift
//  AlgorithmsTest
//
//  Created by Mingming Wang on 6/7/16.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import XCTest

class UnionFindTest: XCTestCase {
  var unionFind: UnionFind!
  
  func testQuickFind() {
    unionFind = QuickFind(size: 10)
    unionFindTest()
  }
  
  func testQuickUnion() {
    unionFind = QuickUnion(size: 10)
    unionFindTest()
  }
  
  func testWeightedQuickUnion() {
    unionFind = WeightedQuickUnion(size: 10)
    unionFindTest()
  }
  
  func testPathCompressionQuickUnion() {
    unionFind = PathCompressionQuickUnion(size: 10)
    unionFindTest()
  }
  
  func testPathCompressionWeightedQuickUnion() {
    unionFind = PathCompressionWeightedQuickUnion(size: 10)
    unionFindTest()
  }
  
  func unionFindTest() {
    unionFind.union(p: 4, q: 3)
    unionFind.union(p: 3, q: 8)
    unionFind.union(p: 6, q: 5)
    unionFind.union(p: 9, q: 4)
    unionFind.union(p: 2, q: 1)
    unionFind.union(p: 8, q: 9)
    unionFind.union(p: 5, q: 0)
    unionFind.union(p: 7, q: 2)
    unionFind.union(p: 6, q: 1)
    unionFind.union(p: 1, q: 0)
    unionFind.union(p: 6, q: 7)
    
    XCTAssertTrue(unionFind.connected(p: 6, q: 5))
    XCTAssertTrue(unionFind.connected(p: 8, q: 9))
    XCTAssertTrue(unionFind.connected(p: 1, q: 0))
    XCTAssertTrue(unionFind.connected(p: 6, q: 7))
    XCTAssertFalse(unionFind.connected(p: 1, q: 9))
  }
}
