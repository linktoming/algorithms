//
//  InsertionSortTest.swift
//  Algorithms
//
//  Created by Mingming Wang on 17/7/16.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import XCTest

class InsertionSortTest: XCTestCase {
  func testExample() {
    var array = randomThousandNumberArray()
    XCTAssertFalse(isSorted(array))
    InsertionSort.sort(&array)
    XCTAssertTrue(isSorted(array))
  }
}
