//
//  KeyIndexedCountingTest.swift
//  Algorithms
//
//  Created by Mingming Wang on 20/08/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import XCTest

class KeyIndexedCountingTest: XCTestCase {

  func testKeyIndexedCounting() {
    var array = randomThousandNumberArray() + randomThousandNumberArray() + randomNumericArray(upTo: 100)
    XCTAssertFalse(isSorted(array))
    KeyIndexedCounting.sort(array: &array, r: 1000)
    XCTAssertTrue(isSorted(array))
  }
}
