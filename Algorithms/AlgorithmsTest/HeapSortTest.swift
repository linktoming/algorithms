//
//  HeapSortTest.swift
//  Algorithms
//
//  Created by Mingming Wang on 04/08/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import XCTest

class HeapSortTest: XCTestCase {

  func testExample() {
    var array = randomThousandNumberArray()
    XCTAssertFalse(isSorted(array))
    HeapSort.sort(&array)
    XCTAssertTrue(isSorted(array))
  }
}
