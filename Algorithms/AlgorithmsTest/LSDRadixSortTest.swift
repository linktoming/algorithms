//
//  LSDRadixSortTest.swift
//  Algorithms
//
//  Created by Mingming Wang on 21/08/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import XCTest

class LSDRadixSortTest: XCTestCase {

  func testLSDRadixSort() {
    var strings = [String]()
    let length = 100
    for _ in 0 ... 10000 {
      let str = randomAlphaNumericString(length: length)
      strings.append(str)
      strings.append(str)
      strings.append(str)
    }
    XCTAssertFalse(isSorted(strings))
    LSDRadixSort.sort(array: &strings, length: length)
    XCTAssertTrue(isSorted(strings))
  }

  func randomAlphaNumericString(length: Int) -> String {

    let allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    let allowedCharsCount = UInt32(allowedChars.characters.count)
    var randomString = ""

    for _ in (0 ..< length) {
      let randomNum = Int(arc4random_uniform(allowedCharsCount))
      let newCharacter = allowedChars[allowedChars.index(allowedChars.startIndex, offsetBy: randomNum)]
      randomString += String(newCharacter)
    }

    return randomString
  }
}
