//
//  SymbolTableTest.swift
//  Algorithms
//
//  Created by Mingming Wang on 06/08/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import XCTest

class SymbolTableTest: XCTestCase {

    func testBinarySearchSymbolTable() {
      let st = BinarySearchSymbolTable<String, Int>()
      st.put(key: "a", value: 1)
      XCTAssertTrue(st.contains(key: "a"))
      XCTAssertTrue(st.get(key: "a") == 1)
      st.delete(key: "a")
      XCTAssertFalse(st.contains(key: "a"))
      XCTAssertTrue(st.get(key: "a") == nil)
      XCTAssertTrue(st.isEmpty())
      st.put(key: "b", value: 2)
      st.put(key: "b", value: 1)
      XCTAssertTrue(st.contains(key: "b"))
      XCTAssertTrue(st.get(key: "b") == 1)
      XCTAssertFalse(st.contains(key: "B"))
      st.put(key: "c", value: 1)
      XCTAssertTrue(st.rank(key: "b") == 0)
      XCTAssertTrue(st.rank(key: "c") == 1)

      var keys = [String]()
      for (key) in st {
        keys.append(key)
      }

      XCTAssertEqual(keys, ["b","c"])
    }

}
