//
//  BinaryIndexedTreeTest.swift
//  Algorithms
//
//  Created by Mingming Wang on 4/9/16.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import XCTest

class BinaryIndexedTreeTest: XCTestCase {
    func testBIT() {
      let bit = BinaryIndexedTree()
      let tree = bit.createTree(input: [1,2,3,4,5,6,7])
      XCTAssertEqual(1, bit.getSum(binaryIndexedTree: tree, index: 0))
      XCTAssertEqual(3, bit.getSum(binaryIndexedTree: tree, index: 1))
      XCTAssertEqual(6, bit.getSum(binaryIndexedTree: tree, index: 2))
      XCTAssertEqual(10, bit.getSum(binaryIndexedTree: tree, index: 3))
      XCTAssertEqual(15, bit.getSum(binaryIndexedTree: tree, index: 4))
      XCTAssertEqual(21, bit.getSum(binaryIndexedTree: tree, index: 5))
      XCTAssertEqual(28, bit.getSum(binaryIndexedTree: tree, index: 6))
    }
}
