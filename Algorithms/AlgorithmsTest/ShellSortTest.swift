//
//  ShellSortTest.swift
//  Algorithms
//
//  Created by Mingming Wang on 24/07/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import XCTest

class ShellSortTest: XCTestCase {

  func testExample() {
    var array = randomThousandNumberArray()
    XCTAssertFalse(isSorted(array))
    ShellSort.sort(&array)
    XCTAssertTrue(isSorted(array))
  }

}
