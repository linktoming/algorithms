//
//  2DBinaryIndexedTreeTest.swift
//  Algorithms
//
//  Created by Mingming Wang on 06/09/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import XCTest

class TwoDimensionBITTest: XCTestCase {

  func testBIT() {
    let bit = TwoDimensionBIT()
    let tree = bit.createTree(input: [[1,2,3,4,5,6,7],[1,2,3,4,5,6,7],[1,2,3,4,5,6,7],[1,2,3,4,5,6,7]])
    
    XCTAssertEqual(1, bit.getSum(binaryIndexedTree: tree, row: 0, col: 0))
    XCTAssertEqual(3, bit.getSum(binaryIndexedTree: tree, row: 0, col: 1))
    XCTAssertEqual(6, bit.getSum(binaryIndexedTree: tree, row: 0, col: 2))
    XCTAssertEqual(10, bit.getSum(binaryIndexedTree: tree, row: 0, col: 3))
    XCTAssertEqual(15, bit.getSum(binaryIndexedTree: tree, row: 0, col: 4))
    XCTAssertEqual(21, bit.getSum(binaryIndexedTree: tree, row: 0, col: 5))
    XCTAssertEqual(28, bit.getSum(binaryIndexedTree: tree, row: 0, col: 6))
    
    XCTAssertEqual(2, bit.getSum(binaryIndexedTree: tree, row: 1, col: 0))
    XCTAssertEqual(6, bit.getSum(binaryIndexedTree: tree, row: 1, col: 1))
    XCTAssertEqual(12, bit.getSum(binaryIndexedTree: tree, row: 1, col: 2))
    XCTAssertEqual(20, bit.getSum(binaryIndexedTree: tree, row: 1, col: 3))
    XCTAssertEqual(30, bit.getSum(binaryIndexedTree: tree, row: 1, col: 4))
    XCTAssertEqual(42, bit.getSum(binaryIndexedTree: tree, row: 1, col: 5))
    XCTAssertEqual(56, bit.getSum(binaryIndexedTree: tree, row: 1, col: 6))
    
    XCTAssertEqual(3, bit.getSum(binaryIndexedTree: tree, row: 2, col: 0))
    XCTAssertEqual(9, bit.getSum(binaryIndexedTree: tree, row: 2, col: 1))
    XCTAssertEqual(18, bit.getSum(binaryIndexedTree: tree, row: 2, col: 2))
    XCTAssertEqual(30, bit.getSum(binaryIndexedTree: tree, row: 2, col: 3))
    XCTAssertEqual(45, bit.getSum(binaryIndexedTree: tree, row: 2, col: 4))
    XCTAssertEqual(63, bit.getSum(binaryIndexedTree: tree, row: 2, col: 5))
    XCTAssertEqual(84, bit.getSum(binaryIndexedTree: tree, row: 2, col: 6))
    
    XCTAssertEqual(4, bit.getSum(binaryIndexedTree: tree, row: 3, col: 0))
    XCTAssertEqual(12, bit.getSum(binaryIndexedTree: tree, row: 3, col: 1))
    XCTAssertEqual(24, bit.getSum(binaryIndexedTree: tree, row: 3, col: 2))
    XCTAssertEqual(40, bit.getSum(binaryIndexedTree: tree, row: 3, col: 3))
    XCTAssertEqual(60, bit.getSum(binaryIndexedTree: tree, row: 3, col: 4))
    XCTAssertEqual(84, bit.getSum(binaryIndexedTree: tree, row: 3, col: 5))
    XCTAssertEqual(112, bit.getSum(binaryIndexedTree: tree, row: 3, col: 6))
    
  }

}
