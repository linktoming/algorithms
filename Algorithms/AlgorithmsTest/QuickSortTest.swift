//
//  QuickSortTest.swift
//  Algorithms
//
//  Created by Mingming Wang on 28/07/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import XCTest

class QuickSortTest: XCTestCase {
  func testQuickSort() {
    var array = randomNumericArray(upTo: 10000)
    QuickSort.sort(&array)
    XCTAssertTrue(isSorted(array))
  }

  func testThreeWayQuickSort() {
    var array = randomNumericArray(upTo: 10000)
    QuickSort.threeWayPartitioning = true
    QuickSort.sort(&array)
    XCTAssertTrue(isSorted(array))
    QuickSort.threeWayPartitioning = false
  }

  func testSelection() {
    var array = randomNumericArray(upTo: 10000)
    let k = 99, i = QuickSort.selection(&array, k: k)
    XCTAssertTrue(i == k)
  }
}
