- elementary implementation for symbol table
- bottom up merge sort
- implement the algorithms learned so far
- check how to insertion sort at one pass in the quick sort
- check the coefficient in sorting algorthms performance

# Further Learning
- Eliminate the copy to the auxiliary array for merge sort (p12 22Mergesort.pdf)
- B-Tree
- Event-Driven Simulation using PQ