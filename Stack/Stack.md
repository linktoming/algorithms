Stack is a FILO data structure that can be implemented by linked list or array 
with resizing. Common operations include `push`, `pop` and `isEmpty`
- linked list implementation
    - keep a reference to the head of the linked list; push and pop from there 
      then update the head reference
    - push and pop take constant time in worse case
    - need extra space for the pointers in linked list
- array with resizing implementation:
    - keep a reference to the index of last item in the array; push and pop from
      there and update the reference. The reference can be current number of 
      items
    - need to impelement resizing logic to handle overflow
      - increase/decrease the size of array by 1 when we push/pop: N^2 running time,
        not practical
      - double the size of the array when it's full. Halve the array when it's one 
        quarter full. -- constant amortized time   
    - every operation takes constant amortized time
    - less space needed than linked list 
