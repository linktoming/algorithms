//
//  File.swift
//  Algorithms
//
//  Created by Mingming Wang on 9/7/16.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

// cannot use protocol due to its current limitation with generics
class Stack<Type>: Sequence {
  func push(_ value: Type) { }
  func pop() -> Type? { return nil }
  func isEmpty() -> Bool { return true }
  func makeIterator() -> AnyIterator<Type> {
    methodUnImplementedError(#function)
  }
}
