//
//  ArrayStack.swift
//  Algorithms
//
//  Created by Mingming Wang on 9/7/16.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

class ArrayStack<Type>: Stack<Type> {
  var items = [Type]()
  override func pop() -> Type? {
    if isEmpty() { return nil }
    let item = items.last
    items.removeLast()
    return item
  }
  
  override func push(_ value: Type) {
    items.append(value)
  }
  
  override func isEmpty() -> Bool {
    return items.isEmpty
  }
  
  override func makeIterator() -> AnyIterator<Type> {
    var nextIndex = items.count - 1
    return AnyIterator {
      if nextIndex < 0 { return nil }
      let resultIndex = nextIndex
      nextIndex = nextIndex - 1
      return self.items[resultIndex]
    }
  }
}
