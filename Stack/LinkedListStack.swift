//
//  LinkedListStack.swift
//  Algorithms
//
//  Created by Mingming Wang on 9/7/16.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

class LinkedListStack<Type>: Stack<Type> {
  var head: LinkedListItem<Type?>?
  override init() {
    head = nil
  }
  
  override func push(_ value: Type) {
    let item = LinkedListItem<Type?>(value: value, next: head)
    head = item
  }
  
  override func pop() -> Type? {
    let value = head?.value
    head = head?.next
    return value
  }
  
  override func isEmpty() -> Bool {
    return head == nil
  }
  
  override func makeIterator() -> AnyIterator<Type> {
    var current = head
    return AnyIterator{
      if current == nil { return nil }
      let result = current
      current = current!.next
      return result!.value
    }
  }
}
