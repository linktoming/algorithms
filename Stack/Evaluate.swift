//
//  Evaluate.swift
//  Algorithms
//
//  Created by Mingming Wang on 16/7/16.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import Foundation
struct Evaluate {
  var expression: String
  var operators: Stack<String>
  var values: Stack<Int>
  init(expression: String) {
    self.expression = expression
    self.operators = LinkedListStack<String>()
    self.values = LinkedListStack<Int>()
  }
  
  func evaluate() -> Int {
    let operators = ["+", "-", "*", "/"]
    for char in expression.characters {
      if operators.contains(String(char)) {
        self.operators.push(String(char))
      } else if let num = Int(String(char)) {
        self.values.push(num)
      } else if char == Character(")") {
        let op = self.operators.pop()!
        var value: Int!
        switch op {
        case "+":
          value = self.values.pop()! + self.values.pop()!
        case "-":
          value = self.values.pop()! - self.values.pop()!
        case "*":
          value = self.values.pop()! * self.values.pop()!
        case "/":
          value = self.values.pop()! / self.values.pop()!
        default:
          break
        }
        self.values.push(value)
      }
    }
    
    return self.values.pop()!
  }
}