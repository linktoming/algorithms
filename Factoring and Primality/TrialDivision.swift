//
//  TrialDivision.swift
//  Algorithms
//
//  Created by Mingming Wang on 30/08/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//
import Darwin
struct TrialDivision {
  static func trialDivision(n: UInt) -> [UInt] {
    if n <= 2 {
      return [n]
    }
    let i = 2
    var factors = [UInt]()
    while Double(i) <= sqrt(Double(n)) {
      
    }
    if i > 1 {
      factors.append(UInt(i))
    }
    return factors
  }

  static func primeNumberNoGreaterThan(n: UInt) -> [UInt] {
    var primes = [UInt]()
    for i in 2 ... n {
      if isPrime(n: i) {
        primes.append(i)
      }
    }
    return primes
  }

  static func isPrime(n: UInt) -> Bool {
    // 1 is neither prime nor composite
    guard n > 1 else { return false }
    var i: UInt = 2
    var isPrime = true
    while Double(i) <= sqrt(Double(n)) {
      if n%i == 0 {
        isPrime = false
        break
      }
      i = i + 1
    }
    return isPrime
  }
}
