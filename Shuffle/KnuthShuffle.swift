//
//  KnuthShuffle.swift
//  Algorithms
//
//  Created by Mingming Wang on 24/07/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import Foundation
struct KnuthShuffle: Shuffle {
  static func shuffle<T>(_ array: inout [T]) {
    for i in 0 ..< array.count {
      let upperBound = i + 1
      let j = arc4random_uniform(UInt32(upperBound))
      swap(&array, first: i, second: Int(j))
    }
  }
}
