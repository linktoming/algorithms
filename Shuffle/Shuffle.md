Shuffle is a problem that given an array of items, output a uniformly randome permutation of it.

One simple algorithm can be assign each item in the array a random number, then sort the random numbers. However, it's based
on sorting algorithms which only provides ~N*LgN performance. We could do better than that.

## Knuth Shuffle
For iteration i, pick a uniformly random value r between 0 and i inclusive, swap array[i] and array[r].

It's similar to insertion sort but instead of comparing the sub array for insertion, just pick a random position for insert.

It provides an uniformly randome permutation of the input array in linear time.

  