//
//  Shuffle.swift
//  Algorithms
//
//  Created by Mingming Wang on 24/07/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import Foundation
protocol Shuffle {
  static func shuffle<T>(_ array:inout [T])
}
