//
//  File.swift
//  Algorithms
//
//  Created by Mingming Wang on 25/7/16.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import Foundation
struct MergeSort: Sort {
  static func sort<T: Comparable>(_ array: inout [T]) {
   var aux = array
   sort(&array, aux: &aux, low: 0, high: array.count - 1)
  }
  
  private static func sort<T: Comparable>(_ array: inout [T], aux: inout [T], low: Int, high: Int) {
    // practical improvement: use insertion sort for small subarrays
    if (high - low + 1 <= 7) {
      InsertionSort.sort(&array, low: low, high: high)
      return 
    }
    if low == high { return }
    let mid = low + (high - low)/2
    sort(&array, aux: &aux, low: low, high: mid)
    sort(&array, aux: &aux, low: mid + 1, high: high)
    // practical improvement: stop at merging if the whole array is already sorted
    if less(first: array[mid], second: array[mid + 1]) { return }
    merge(&array, aux: &aux, low: low, mid: mid, high: high)
  }
}
