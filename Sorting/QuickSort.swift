//
//  QuickSort.swift
//  Algorithms
//
//  Created by Mingming Wang on 28/07/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import Foundation
struct QuickSort: Sort {
  static var threeWayPartitioning = false
  static func sort<T : Comparable>(_ array: inout [T]) {
    KnuthShuffle.shuffle(&array)
    if threeWayPartitioning {
      threeWayQuickSort(&array, low: 0, high: array.count - 1)
    } else {
      sort(&array, low: 0, high: array.count - 1)
    }
  }

  private static func sort<T: Comparable>(_ array:inout [T], low: Int, high: Int) {
    if low >= high { return }
    // Practical improvement -- use insertion sort for small array
    if (high <= low + 10 - 1) {
      InsertionSort.sort(&array, low: low, high: high)
    }

    // Practical improvement -- use median of random three to make partitioning item more close to median
    // of the input.
    let median = medianOfThreeIndex(array, i: low, j: low + (high - low)/2, k: high)
    swap(&array, first: median, second: low)

    let j = partition(&array, low: low, high: high)
    sort(&array, low: low, high: j - 1)
    sort(&array, low: j + 1, high: high)
  }

  fileprivate static func partition<T : Comparable>(_ array:inout [T], low: Int, high: Int) -> Int {
    var i = low + 1
    var j = high
    while true {
      while (less(first: array[i], second: array[low])) {
        i = i + 1
        if i > j {
          break
        }
      }

      while (less(first: array[low], second: array[j])) {
        j = j - 1
        if i > j {
          break
        }
      }

      if i < j {
        swap(&array, first: i, second: j)
        i = i + 1
        j = j - 1
      } else {
        break
      }
    }

    swap(&array, first: j, second: low)
    return j
  }

  fileprivate static func threeWayQuickSort<T : Comparable>(_ array:inout [T], low: Int, high: Int) {
    if high <= low {
      return
    }
    // i: current checking item
    // lt: the item before lt is less than current item
    // gt: the item after gt is greater than current item
    // the items between lt (inclusive) and i (exclusive) are equal to current item
    var i = low + 1, lt = low, gt = high
    // common mistake: use array[low] directly as in two way Quicksort
    let p = array[low]
    while i <= gt {
      if array[i] < p {
        swap(&array, first: i, second: lt)
        i = i + 1
        lt = lt + 1
      } else if array[i] > p {
        swap(&array, first: i, second: gt)
        gt = gt - 1
      } else {
        i = i + 1
      }
    }

    threeWayQuickSort(&array, low: low, high: lt - 1)
    threeWayQuickSort(&array, low: gt + 1, high: high)
  }
}

extension QuickSort {
  // return the kth smallest item, start from 0
  static func selection<T : Comparable>(_ array: inout [T], k: Int) -> T? {
    if k < 0 || k >= array.count { return nil }
    KnuthShuffle.shuffle(&array)
    var low = 0, high = array.count - 1
    while (low < high) {
      let p = partition(&array, low: low, high: high)
      if p > k {
        high = p - 1
      } else if p < k {
        low = p + 1
      } else {
        return array[p]
      }
    }
    return array[k]
  }

  fileprivate static func medianOfThreeIndex<T: Comparable>(_ array: [T], i: Int, j: Int, k: Int) -> Int{
    var min = i
    if less(first: array[j], second: array[min]) {
      min = j
    }
    if less(first: array[k], second: array[min]) {
      min = k
    }
    var max = i
    if less(first: array[max], second: array[j]) {
      max = j
    }

    if less(first: array[max], second: array[k]) {
      max = k
    }

    return i + j + k - min - max
  }
}
