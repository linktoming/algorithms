//
//  SelectionSort.swift
//  Algorithms
//
//  Created by Mingming Wang on 17/7/16.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

struct SelectionSort: Sort {
  static func sort<T : Comparable>(_ array: inout [T]) {
    for i in 0 ..< array.count {
      var min = i
      for j in i + 1 ..< array.count {
        if less(first: array[j], second: array[min]) {
          min = j
        }
      }
      swap(&array, first: i, second: min)
    }
  }
}
