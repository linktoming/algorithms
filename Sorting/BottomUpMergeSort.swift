//
//  BottomUpMergeSort.swift
//  Algorithms
//
//  Created by Mingming Wang on 28/07/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import Foundation
struct BottomUpMergeSort: Sort {
  static func sort<T : Comparable>(_ array: inout [T]) {
    var aux = array
    var size = 1
    while (size < array.count) {
      for i in stride(from: 0, to: array.count - size, by: size * 2) {
        merge(&array, aux: &aux, low: i, mid: i + size - 1, high: min(i + 2*size - 1, array.count - 1))
      }
      size = size + size
    }
  }
}
