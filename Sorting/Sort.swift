//
//  SortingHelper.swift
//  Algorithms
//
//  Created by Mingming Wang on 17/7/16.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//
protocol Sort {
  static func sort<T: Comparable>(_ array: inout [T])
}
extension Sort {
  static func merge<T: Comparable>(_ array: inout [T], aux: inout [T], low: Int, mid: Int, high: Int) {
    assert(isSorted(Array(array[low...mid])))
    assert(isSorted(Array(array[mid + 1...high])))
    for i in low ... high {
      aux[i] = array[i]
    }
    var i = low, j = mid + 1
    for current in low...high {
      if (i > mid) {
        array[current] = aux[j]
        j = j + 1
      } else if (j > high) {
        array[current] = aux[i]
        i = i + 1
      } else if less(first: aux[i], second: aux[j]) {
        array[current] = aux[i]
        i = i + 1
      } else {
        array[current] = aux[j]
        j = j + 1
      }
    }
    assert(isSorted(Array(array[low...high])))
  }
}

