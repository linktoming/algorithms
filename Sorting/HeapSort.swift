//
//  HeapSort.swift
//  Algorithms
//
//  Created by Mingming Wang on 03/08/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//
struct HeapSort: Sort {
  static func sort<T : Comparable>(_ array: inout [T]) {
    var size = array.count
    var p = size/2
    while p >= 1 {
      sink(&array, index: p, size: size)
      p = p - 1
    }
    while size > 1 {
      swap(&array, first: 1, second: size)
      size = size - 1
      sink(&array, index: 1, size: size)
    }
  }

  private static func sink<T: Comparable>(_ array:inout [T], index: Int, size: Int) {
    var current = index
    while current * 2 <= size {
      var j = current * 2
      if j < size && less(array, first: j, second: j + 1) {
        j = j + 1
      }
      if less(array, first: current, second: j) {
        swap(&array, first: current, second: j)
        current = j
      } else {
        break
      }
    }
  }

  private static func less<T: Comparable>(_ array: [T], first: Int, second: Int) -> Bool {
    return array[first - 1] < array[second - 1]
  }

  private static func swap<T>(_ array: inout [T], first: Int, second: Int) {
    let temp = array[first - 1]
    array[first - 1] = array[second - 1]
    array[second - 1] = temp
  }
}
