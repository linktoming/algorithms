//
//  InsertionSort.swift
//  Algorithms
//
//  Created by Mingming Wang on 17/7/16.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

struct InsertionSort: Sort {
  static func sort<T : Comparable>(_ array: inout [T]) {
    for i in 1 ..< array.count {
      for j in (1...i).reversed() {
        if less(first: array[j], second: array[j-1]) {
          swap(&array, first: j, second: j - 1)
        } else {
          break
        }
      }
    }
  }

  static func sort<T : Comparable>(_ array: inout [T], low: Int, high: Int) {
    let start = low + 1
    for i in start ... high {
      for j in (start...i).reversed() {
        if less(first: array[j], second: array[j-1]) {
          swap(&array, first: j, second: j - 1)
        } else {
          break
        }
      }
    }
  }
}
