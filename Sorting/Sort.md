Sorting, as the name indicates, is to put a given sequence of values
in order. 

## Selection Sort
Selection sort is a basic sorting method that scans the array from left to right,
select the *i*th smallest item ( 0<= *i* < array size ) and put it into place 
in each pass. After all passes, the array is in order.

It's like how we sort a hand of poker -- first we select the smallest card and put 
in the left most position in the hand, then we select the second smallest card and
put it into the second left most position, and so on. 

Although it's performance is ~ 1/2*N^2 (compares), it takes N, linear number of 
exchanges of items. It's running time is insensive to input while data movement 
is minimal.

## Insertion Sort
Insertion sort is another basic sorting method that takes ~N^2 time. Its way is scaning
the array from left to right, find the right position for the item in each pass.
It's like we have one hand of sorted cards, when we pick one from the deck, we 
find a position to insert in the hand of cards, and so on. After we pick all the cards
in the deck and insert them in position each time, we get all the cards sorted.

Insertion sort uses ~1/4\*N^2 compares and ~1/4\*N^2 exchanges on average for
randomly ordered array with distinc keys. 

Best case when the array is in assending order already, insertion sort takes N-1
compares and 0 exchanges. So when the array is more or less sorted, insertion sort
tends to have a good performance.

Worse case when the array is in descending order with distinct keys, insertion
sort takes ~1/2\*N^2 compares and ~1/2\*N^2 exchanges.

## Shell Sort
The idea of shell sort is to move entries more than one position at a time by h-sorting the array.
After h-sort the array for decreasing sequence of values of h from x to 1, the array is sorted.

The good candidate to h-sort an array is insertion sort because 
- if h is large, the subarray is small, thus insertion sort is fast
- if h is small, the subarray is almost sorted, thus insertion sort is also fast

Shell sort is based on the theory that `a g-sorted array remains g-sorted after h-sorting it`

Possible requences of h to use:
- power of 2: 1, 2, 4, 8, 16, 32... -> No. Because in passes, some same element comparision happens again and again and not sorting any items.
- Power of 2 minus 1: 1, 3, 7, 15, 31 -> Maybe
- 3*x + 1 -> 1, 4, 13, 40, 121...OK. Easy to compute
- Sedgewick Sequence: Good but not easy to compute.

The worst case number of compares used by shell sort using 3x+1 increments is O(N^(3/2)) while the accurate model has not been
discovered. 

In practice, shell sort is fast unless array size is huge. It has tiny, fixed footprint for code thus is used in some embeded
systems.

## Merge Sort
Merge sort and quick sort are another two classic sorting algorithms. The full scientific understanding of them has enabled us
to develop them into practical system sort. Quick sort is even honoured as one of the top 10 algorithms of 20th century in 
sceience and engineering.

Java sort for objects is using merge sort while Java sort for primative types is using quick sort. They are also seen in other
system or language implementations of sorting algorithms.


Merge sort is a recursive divide and conque algorithm as following:
- divive the array in two halves
- recursively sort each half
- merge two halves

Merge sort uses at most N\*LogN compares and 6N\*LogN array accesses to sort an array of size N. It uses extra space proportional to N. 

For recursive divide and conque algorithms, if D(N) = 2\*D(N/2) + N, with D(1) = 0, then D(N) = N\*LogN
 
### Practical Improvement of Merge Sort
- Use insertion sort for small sub array
  - merge sort has too much over head for tiny subarrays 
  - cut off size for insert sort at ~ 7 items
- Stop at merging when the array is already sorted
  - check whether the array is already sorted before start merging. 
    If the largest item in first half is smaller than the smallest item in the second half.
  - useful for partially sorted array

### Bottom up Merge Sort  
Bottom up merge sort, instead of merging recursively, merge the array in an iterative manner.
- pass through the array, merge subarray of size 1
- repeat for subarrays of size 2, 4, 8, 16, ...

It's a simple and non recursive implementation of merge sort that is about  10% slower than recursive, top-down mergesort on typical systems.

## Quick Sort 
The basic steps for quick sort is
- shuffle the array
- partition the array so that for some j 
  - entry a[j] is in place
  - no larger entry to the left of j
  - no smaller entry to the right of j
- sort the left and righ subarray recursively from step 2

Th main logic for quick sort is the partition process. 
- scan i from left to right so long as a[i] < a[low]
- scan j from right to left so long as a[j] > a[low]
- exchange a[i] with a[j]
- repeat the above process until i and i pointers cross.

### Performance Characters
- In the best case the number of compares is ~N\*LogN
- In the worse case the number of compares is ~1/2*N^2, which is even rarer than the chance of our computer is stuck by lightning bolt
- The verage number of compares in quick sort of an array of N distinct keys is ~2N\*LnN ~= 1.39N\*LgN (while the number of exchanges is ~ 1/3N*LnN), which is about 0.39 slower than merge sort.

But practically quick sort is faster due to less data movement. Random shuffle is the probabilistic guarantee against worst case.

Quicksort is an in-place sorting algorithm but not stable.
- partitioning use constant extra space
- depth of recursion uses logarithmic extra space (with high probability). It can guarantee logrithmic depth by recurring on smaller subarray 
  before larger array. 

### Practical Improvement
Insertion sort for small subarray.
- Even quicksort has too much overhead for tiny subarrays
- Cutoff to insertion sort for ~ 10 items
- Note: could delay insertion sort until one pass at end.

Use median of sample to get better choice of pivot item
- Best choice of pivot item = median
- Estimate true median by taking median of samples
- Median of 3 (random) items -- ~ 12/7 N\*LnN compares (slightly fewer) and ~ 12/35 N\*LnN exchanges (slightly more)

### Selection Problem
Given an array of items, find a kth smallest item.
- k = 0 -> smallest item
- k = N - 1 -> largest item
- k = N/2 -> median 

Use existing theory as a guide, we can easily have the following conclution
- N\*LogN upper bound -- can just use sort to solve this problem
- N upper bound for k = 1, 2, 3 -- for small k, we can just loop through the array and find
- N lower bound -- must check every item so that the result considered every item

#### Questions
- is selection as hard as sorting with N\*LogN lower bound? -- theoretically NO.
- is there N upper bound solution for selection problem? -- 

Computer scientists have found linear algorithm for selection problem but the constant time is too high for practical use.
Thus it's still worthwhile to seek practical linear-time (worse case) algorithm. Until one is discovered, use quick-select if 
you don't need a full sort. 

#### Quick-select 
Selection based on Quicksort's partition logic:
- partition the array so that array[j] is in place.
- if j = k, return array[k] else repeat search on subarray on one side of array[j] depends on j and k's relative order.

Quick-sort takes linear time on average. 
- Intuitively, each partitioning step splits the array approximately in half. N + N/2 + N/4 + ...+ 1 ~ 2N

It uses ~ 1/2N^2 compares in worst case but the random shuffle provides propabilistic guarantee against that.

### Duplicate Keys
The practical application of sorting algorithms sometimes contains duplicated keys. For example, we may want sort population by age,
remove duplication in a mail list, sorting students by department, etc. Usually such applications have a huge array with small number of 
keys.

Mergesort with duplicated keys uses between 1/2N\*LgN and NLgN compared.

Quicksort with duplicate keys goes quadratic unless partitioning stops on equal keys.
- Mistake: puting all items euqal to partitioning items on one side, which results in ~1/2N^2 compares.
- Recommended: stoping scans on items equal to partitioning key, which will use ~N\*LgN compares when all keys are equal.
- Desirable: putting all items equal to partitioning key in place.

### 3-way QuickSort
Partition the array into 3 parts so that
- entries between lt and gt equal to partition item v.
- no larger entries to the left of lt.
- no smaller entries to the right of gt.

Dijkstra 3-way partitioning
- let v to partitioning item a[lo]
- scan i from left to right
  - for a[i] < v: exchange a[lt] and a[i], increase both i and lt
  - for a[i] > v: exchange a[i] and a[gt], decrease gt
  - for a[i] == v: increase i
The items between lt (inclusive) and i (exclusive) are equal to the partition item.  

Quicksort with 3-way partitioning is entropy-optimal. Randomized quicksort with 3-way partitioning reduces 
running time from linearithmic to linear in broad class of applications.

## Heap Sort
Heap sort is using binary heap processing internally. 
1. First, it constructs binary heap in place in a bottom up approch. 
2. Second, it get its maxmium key and put into position iteratively.
    - it puts its maxmium key in position by exchanging the key at the root and the last key
    - then it sinks the last key at the root so that it recovers its heap order.

Heap construction uses <= 2N compares and exchanges while heap sort uses <= 2N\*lgN compares and exchanges.

The significance of heap sort is it's a inplace sorting algorithm that guarantee N*lgN worst case.
It's optimal for both space and time but in practice heap sort is not used that much due to the 
following drawbacks:  
- its inner loop is larger than quick sort
- make poor use of cache memory in mordern systems (refering memory that's far away from current 
  position rather than the memory nearby like quick sort or memory just referred to to utilize
  cache mechanism)
- not stable (people may choose to use merge sort in practice because of its stability)


## Properties of Sorting Algorithms
### In Place Sorting
A sorting algorithm is in place if it uses <= c\*LogN extra memory.

Insertion sort, selection sort and shell sort is in place. Merge sort's classic implementation is not in place.

### Stability
A stable sort reserves the relative order of items with equal keys.

Insertion sort and merge sort are stable. Selection sort and shell sort are not stable.

*Need to check code carefully -- use `less than` rather than `less than or equal to`*. 

## Sorting Summary
Some quick fact on sorting methods performance:
- Lower bound for sorting is N\*LgN.    
- Upper bound provided by merge sort is N\*LgN which is an optimal sorting method   
- Quick sort provides amotized performance N\*LgN on average although its worse 
  case would be N^2.  

Below is the summary of sorting algorithms

||Inplace?|Stable?|Worst|Average|Best|Remarks|
|:---|:---:|:---:|:---:|:---:|:---:|:---|
|selection|✔︎||N^2/2|N^2/2|N^2/2|N exchanges|
|insertion|✔|✔|N^2/2|N^2/4|N|use for small N or partially ordered list|
|shell|✔||?|?|N|tight code, subquadratic|
|merge||✔|N\*lgN|N\*lgN|N\*lgN|N\*lgN guarantee, stable|
|quick|✔||N^2/2|2N\*lnN|N\*lgN|N\*lgN probabilistic guarantee, fatest in practice|
|3-way quick|✔||N^2/2|2N\*lnN|N|improves quicksort in presence of duplicate keys|
|heap|✔||2N\*lgN|2N\*lgN|N\*lgN|N\*lgN guarantee, in place|
|???|✔|✔|N\*lgN|N\*lgN|N|holy sorting grail|
