//
//  WeightedQuickUnion.swift
//  Algorithms
//
//  Created by Mingming Wang on 6/7/16.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

class WeightedQuickUnion: QuickUnion {
  private var treeSize: [Int]
  required init(size: Int) {
    treeSize = [Int]()
    for _ in 0 ..< size {
      treeSize.append(1)
    }
    super.init(size: size)
  }
  
  override func union(p: Int, q: Int) {
    let rootP = root(p: p)
    let rootQ = root(p: q)
    if treeSize[rootP] < treeSize[rootQ] {
      parent[rootP] = rootQ
      treeSize[rootQ] = treeSize[rootP] + treeSize[rootQ]
    } else {
      parent[rootQ] = rootP
      treeSize[rootP] = treeSize[rootP] + treeSize[rootQ]
    }
  }
}
