//
//  UnionFind.swift
//
//
//  Created by Mingming Wang on 04/07/2016.
//
//

protocol UnionFind {
  init(size: Int)                             // initialize union-find data structure with size
  mutating func union(p: Int, q: Int)       // add connection between p and q
  func connected(p: Int, q: Int) -> Bool    // are p and q connected? in the same component
  // func find(p: Int) -> Int                 // component identifier for p
  // func count() -> Int                      // number of components
}
