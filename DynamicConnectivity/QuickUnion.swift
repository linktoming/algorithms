//
//  QuickUnionUF.swift
//  Algorithms
//
//  Created by Mingming Wang on 6/7/16.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

class QuickUnion: UnionFind {
  var parent: [Int]
  required init(size: Int) {
    parent = [Int]()
    for i in 0 ..< size {
      parent.append(i)
    }
  }
  
  func connected(p: Int, q: Int) -> Bool {
    return root(p: p) == root(p: q)
  }
  
  func union(p: Int, q: Int) {
    let rootP = root(p: p)
    parent[rootP] = root(p: q)
  }
  
  func root(p:Int) -> Int {
    var item = p
    while item != parent[item] {
      item = parent[item]
    }
    return item
  }
}
