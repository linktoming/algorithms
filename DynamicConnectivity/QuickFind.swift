//
//  QuickFind.swift
//  Algorithms
//
//  Created by Mingming Wang on 6/7/16.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

struct QuickFind: UnionFind {
  // p and q is connected if they have the same id, id[p] == id[q]
  private var id: [Int]
  init(size: Int) {
    id = [Int]()
    for i in 0 ..< size {
      id.append(i)
    }
  }
  
  mutating func union(p: Int, q: Int) {
    if !connected(p:p, q: q) {
      print("union: connecting \(p) and \(q)")
      let idP = id[p]
      let idQ = id[q]
      for i in 0 ..< id.count {
        if id[i] == idP { id[i] = idQ }
      }
    } else {
      print("union: \(p) and \(q) are already connected")
    }
  }
  
  func connected(p: Int, q: Int) -> Bool {
    return id[p] == id[q]
  }
}
