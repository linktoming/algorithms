//
//  PathCompressionQuickUnion.swift
//  Algorithms
//
//  Created by Mingming Wang on 6/7/16.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

class PathCompressionQuickUnion: QuickUnion {
  override func root(p: Int) -> Int {
    var item = p
    while item != parent[item] {
      parent[item] = parent[parent[item]]
      item = parent[item]
    }
    return item
  }
}
