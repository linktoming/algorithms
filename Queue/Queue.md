Queue is a FIFO data structure that can be implemented by linked list or array with
resizing. Common operations include `enqueue`, `dequeue` and `isEmpty`.
- linked list implementation keeps reference to the head and tail of the list 
  and enqueue and dequeue from the opposite end of the list. Normally we can
  dequeue from the head and enqueue from the tail. Then update the references
  accordingly.
- array imlementation with resizing: resizing logic is similar to stack implementation
  but a bit more complex. It also keeps references to the first and last item in the
  array and dequeue and enqueue from opposite side of the array. Normally we enqueue
  from the end of the array and dequeue from the begining. The simple resizing 
  logic I could think out is like the following:
    - When dequeue, the halving logic is similar to stack's. When the array is one 
      quarter full, we halve the array. It will keep the array always no less than
      one quarter full.
    - When enqueue and the index of last item reaches the end of the array, we'll 
      always need to move items to make space for the subsequent items. We examine
      how full the array is. If it's full, we double the size. If it's one quarter
      full to half full, we don't resize the array, only moving the items to the
      beginning of the array. If it's from half full to full both non inclusive, 
      we can either move the items to the begining or double the size of the array.
      We'll need anaysis to see which strategy is better.
        - Maybe we can do this: if it's one quarter full to three quarter full, we move the 
          items instead of double the size of array. If it's three quarter full to full,
          we double the size of the array and move the items to the beginning. 
- the pros and cons of linked list and array with resizing implementation are same
  as the implementations for stack. Linked list provides constant dequeue and enqueue
  time while needs extra space for pointers. Array with resizing takes less space
  than linked list implementation while provides amortized constant time.

