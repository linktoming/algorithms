//
//  LinkedListQueue.swift
//  Algorithms
//
//  Created by Mingming Wang on 9/7/16.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

class LinkedListQueue<T>: Queue<T> {
  var head: LinkedListItem<T>?
  var tail: LinkedListItem<T>?
  
  override func enqueue(_ item: T) {
    let newTail = LinkedListItem(value: item, next: nil)
    tail?.next = newTail
    tail = newTail
    if isEmpty() { head = newTail }
  }
  
  override func dequeue() -> T? {
    if isEmpty() { return nil }
    let item = head?.value
    head = head?.next
    if isEmpty() { tail = nil }
    return item
  }
  
  override func isEmpty() -> Bool {
    return head == nil
  }
  
  override func makeIterator() -> AnyIterator<T> {
    var current = head
    return AnyIterator{
      if current == nil { return nil }
      let result = current
      current = current!.next
      return result!.value
    }
  }
}
