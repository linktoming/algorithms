//
//  Queue.swift
//  Algorithms
//
//  Created by Mingming Wang on 9/7/16.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

class Queue<T>: Sequence {
  func enqueue(_ item: T) {
    methodUnImplementedError(#function)
  }
  
  func dequeue() -> T? {
    methodUnImplementedError(#function)
  }
  
  func isEmpty() -> Bool {
    methodUnImplementedError(#function)
  }
  
  func makeIterator() -> AnyIterator<T> {
    methodUnImplementedError(#function)
  }
  
  private func notImplemented() {
    methodUnImplementedError(#function)
  }
}
