//
//  ArrayQueue.swift
//  Algorithms
//
//  Created by Mingming Wang on 9/7/16.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import Foundation
class ArrayQueue<T>: Queue<T> {
  var items = [T]()
  override func enqueue(_ item: T) {
    items.append(item)
  }
  
  override func dequeue() -> T? {
    if isEmpty() { return nil }
    return items.removeFirst()
  }
  
  override func isEmpty() -> Bool {
    return items.isEmpty
  }
  
  override func makeIterator() -> AnyIterator<T> {
    var current = 0
    return AnyIterator {
      if (self.items.count - 1) < current { return nil }
      let resultIndex = current
      current = current + 1
      return self.items[resultIndex]
    }
  }
}
