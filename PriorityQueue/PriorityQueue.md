Priority Queue (PQ) is one of the collection type (data structure that supports insering and deleting items in a specific order, 
e.g. stack, queue, bag, random queue) which will remove the (or smallest if it is minimium priority queue) item of largest priority 
for deleting operation.

It's a generalization of collection type since we can define the priority according to the data structure we need.

One application of priority queue is to find the largest M items in a stream of N items, where N is too large to store in computer memory 
all at once. We can use minium priority queue to keep a refence of the largest M items so far -- every time we insert an item into the queue and 
its size is more than M, remove the minimium item. We can use different algorithms to solve this problem with the following performance matrix:
|implementation|time|space|
|:---:|:---|:---|
|sort|N\*lgN|N|
|elementary PQ|MN|M|
|binary heap|N\*lgM|M|
|best in theory|N|M|

## Priority Queue Implementations
|implementation|insert|delete max|max|
|:---:|:---|:---|:---|
|unordered array|1|N|N|
|ordered array|N|1|1|
|binary heap|log N|log N|1|
## Binary Heap
A binary heap is a complete binary tree structure that satifies the heap ordering property.
- binary tree: empty or node with links to left and right binary trees
- complete tree: perfectly balanced tree, except for bottom level
- heap ordering: the value of each node is less (greater than for minimium priority queue) than or equal to 
  the value of its parents, with the maximium-value (minimium-value for minimium priority queue) element at the root.

Heigh of a complete tree with N nodes is |\_lgN\_| since the heigh only increases when N is a power of 2.
### Array Representation of Binary Heap
Binary heaps are commonly implemented using arrays while theirselves are commonly used to implement priority queues.
- indices start with 1
- take nodes in level order
- largest key is array[1] for maximium tree
- compact (since the tree is complete) and no explicit links needed
  - children of node at k are at 2k and 2k+1
  - parent of node k is at k/2





