//
//  PriorityQueue.swift
//  Algorithms
//
//  Created by Mingming Wang on 02/08/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import Foundation
class MaxPriorityQueue<T: Comparable> {

  func insert(_ item: T) {
    methodUnImplementedError(#function)
  }

  func deleteMax() -> T {
    methodUnImplementedError(#function)
  }

  func isEmpty() -> Bool {
    return size() == 0
  }

  // return the largest key
  func pq_max() -> T {
    methodUnImplementedError(#function)
  }
  // returnt the number of entries in the priority queue
  func size() -> Int {
    methodUnImplementedError(#function)
  }
}
