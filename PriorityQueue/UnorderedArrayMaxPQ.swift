//
//  UnOrderedArrayMaxPQ.swift
//  Algorithms
//
//  Created by Mingming Wang on 02/08/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import Foundation
class UnorderedArrayMaxPQ<T: Comparable>: MaxPriorityQueue<T> {
  private var array = [T]()

  override func insert(_ item: T) {
    array.append(item)
  }

  override func deleteMax() -> T {
    if !isEmpty() {
      var max = array.startIndex
      for (index, item) in array.enumerated() {
        if array[max] < item {
          max = index
        }
      }
      swap(&array, first: max, second: array.endIndex - 1)
      return array.removeLast()
    } else {
      fatalError("Tring to delete item in an empty priority queue")
    }
  }
  
  override func size() -> Int {
    return array.count
  }
}
