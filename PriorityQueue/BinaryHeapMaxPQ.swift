//
//  BinaryHeapMaxPQ.swift
//  Algorithms
//
//  Created by Mingming Wang on 02/08/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import Foundation
class BinaryHeapMaxPQ<T: Comparable>: MaxPriorityQueue<T> {
  fileprivate var array = [T]() // using item starting with index 1 for simplicity

  override func insert(_ item: T) {
    // Insert the item twice for first item and use the item at index 1 so that
    // the array starts at index 1 effectively.
    if isEmpty() {
      array.append(item)
    }
    array.append(item)
    swim(array.endIndex - 1)
  }

  override func deleteMax() -> T {
    if isEmpty() {
      fatalError("trying to delete item in an empty priority queue")
    }

    let v = array[1]
    swap(&array, first: 1, second: array.endIndex - 1)
    array.removeLast()
    sink(1)
    return v
  }

  override func size() -> Int {
    return max(array.count - 1, 0)
  }

  override func pq_max() -> T {
    if isEmpty() {
      fatalError("trying to access max item in an empty priority queue")
    }
    return array[1]
  }
}
extension BinaryHeapMaxPQ {
  fileprivate func swim(_ index: Int) {
    var current = index
    while (current > 1 && less(first: array[current/2], second: array[current])) {
      swap(&array, first: current/2, second: current)
      current = current/2
    }
  }

  fileprivate func sink(_ index: Int) {
    var current = index
    while current * 2 <= size() {
      var j = current * 2
      if j < size() && less(first: array[j], second: array[j+1]) {
        j = j + 1
      }

      if less(first: array[current], second: array[j]) {
        swap(&array, first: current, second: j)
        current = j
      } else {
        break
      }
    }
  }
}
