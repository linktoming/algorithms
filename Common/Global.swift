//
//  Global.swift
//  Algorithms
//
//  Created by Mingming Wang on 30/07/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

import Foundation
func methodUnImplementedError(_ method: String) -> Never  {
  fatalError("\(method) is not implemented")
}

func swap<T>(_ array: inout [T], first: Int, second: Int) {
  let temp = array[second]
  array[second] = array[first]
  array[first] = temp
}

func less<T: Comparable>(first: T, second: T) -> Bool {
  return first < second
}

func isSorted<T: Comparable>(_ array:[T]) -> Bool {
  for i in 1 ..< array.count {
    if less(first: array[i], second: array[i-1]) { return false }
  }
  return true
}
