//
//  BinaryIndexedTree.swift
//  Algorithms
//
//  Created by Mingming Wang on 4/9/16.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//
//  References:
//  http://www.geeksforgeeks.org/binary-indexed-tree-or-fenwick-tree-2/
//  https://www.topcoder.com/community/data-science/data-science-tutorials/binary-indexed-trees/
//  http://en.wikipedia.org/wiki/Fenwick_tree
//  https://www.youtube.com/watch?v=CWDQJGaN1gY
//  https://github.com/mission-peace/interview/blob/master/src/com/interview/tree/FenwickTree.java
//  https://www.youtube.com/watch?v=v_wj_mOAlig
//  https://leetcode.com/problems/range-sum-query-2d-mutable/

class BinaryIndexedTree {
  func createTree(input: [Int]) -> [Int] {
    var binaryIndexedTree = [Int](repeating: 0, count: input.count + 1)
    for (index, value) in input.enumerated() {
      update(binaryIndexedTree: &binaryIndexedTree, val: value, index: index)
    }
    return binaryIndexedTree
  }
  
  func update(binaryIndexedTree: inout [Int], val: Int, index: Int) {
    var index = index + 1
    while index < binaryIndexedTree.count {
      binaryIndexedTree[index] = binaryIndexedTree[index] + val
      index = nextIndex(index: index)
    }
  }
  
  func getSum(binaryIndexedTree: [Int], index: Int) -> Int {
    var index = index + 1
    var sum = 0
    while index != 0 {
      sum = binaryIndexedTree[index] + sum
      index = parentIndex(index: index)
    }
    return sum
  }
  
  private func nextIndex(index: Int) -> Int {
    return index + (index & -index)
  }
  
  private func parentIndex(index: Int) -> Int {
    return index - (index & -index)
  }
  
}
