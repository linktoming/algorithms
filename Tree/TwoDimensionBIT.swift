//
//  2DBinaryIndexedTree.swift
//  Algorithms
//
//  Created by Mingming Wang on 4/9/16.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//
//  References:
//  http://www.geeksforgeeks.org/binary-indexed-tree-or-fenwick-tree-2/
//  https://www.topcoder.com/community/data-science/data-science-tutorials/binary-indexed-trees/
//  http://en.wikipedia.org/wiki/Fenwick_tree
//  https://www.youtube.com/watch?v=CWDQJGaN1gY
//  https://github.com/mission-peace/interview/blob/master/src/com/interview/tree/FenwickTree.java
//  https://www.youtube.com/watch?v=v_wj_mOAlig
//  https://leetcode.com/problems/range-sum-query-2d-mutable/

class TwoDimensionBIT {
  func createTree(input: [[Int]]) -> [[Int]] {
    let row = input.count
    guard row > 0 else { return [[Int]]() }
    let col = input[0].count
    guard col > 0 else { return [[Int]]() }

    var twoDimensionBIT: [[Int]] = [[Int]](repeating: [Int](repeating: 0, count: col + 1), count: row + 1)

    for (row, rowValue) in input.enumerated() {
      for (col, colValue) in rowValue.enumerated() {
        update(binaryIndexedTree: &twoDimensionBIT, val: colValue, row: row, col: col)
      }
    }
    return twoDimensionBIT
  }
  
  func update(binaryIndexedTree: inout [[Int]], val: Int,  row: Int, col: Int) {
    var row = row + 1
    let startCol = col + 1
    while row < binaryIndexedTree.count {
      var col = startCol
      while col < binaryIndexedTree[row].count {
        binaryIndexedTree[row][col] = binaryIndexedTree[row][col] + val
        col = nextIndex(index: col)
      }
      row = nextIndex(index: row)
    }
  }
  
  func getSum(binaryIndexedTree: [[Int]], row: Int, col: Int) -> Int {
    var sum = 0
    var row = row + 1
    let startCol = col + 1
    while row > 0 && row < binaryIndexedTree.count {
      var col = startCol
      while col > 0 && col < binaryIndexedTree[row].count {
        sum = sum + binaryIndexedTree[row][col]
        col = parentIndex(index: col)
      }
      row = parentIndex(index: row)
    }
    return sum
  }
  
  private func nextIndex(index: Int) -> Int {
    return index + index & (-index)
  }
  
  private func parentIndex(index: Int) -> Int {
    return index - index & (-index)
  }
  
}
