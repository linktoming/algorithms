//
//  LinkedList.swift
//  Algorithms
//
//  Created by Mingming Wang on 9/7/16.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

class LinkedListItem<Type> {
  var value: Type
  var next: LinkedListItem<Type>?
  init(value: Type, next: LinkedListItem<Type>?) {
    self.value = value
    self.next = next
  }
}