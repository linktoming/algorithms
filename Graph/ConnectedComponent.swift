//
//  ConnectedComponent.swift
//  Algorithms
//
//  Created by Mingming Wang on 18/08/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

class ConnectedComponent {
  private var ids: [Int?]
  private var marked: [Bool]
  private var _count = 0
  init(graph: Graph) {
    marked = [Bool](repeating: false, count: graph.V())
    ids = [Int?](repeating: nil, count: graph.V())
    for v in 0 ..< graph.V() {
      if !marked[v] {
        dfs(graph, v)
        _count = _count + 1
      }
    }
  }

  func connected(v: Int, w: Int) -> Bool {
    return ids[v]! == ids[w]!
  }

  func count() -> Int {
    return _count
  }

  func id(v: Int) -> Int {
    return ids[v]!
  }

  private func dfs(_ graph: Graph, _ s: Int) {
    marked[s] = true
    ids[s] = _count
    for v in graph.adjacentTo(v: s) {
      if !marked[v] {
        dfs(graph, v)
      }
    }
  }
}
