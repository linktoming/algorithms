//
//  Digraph.swift
//  Algorithms
//
//  Created by Mingming Wang on 24/08/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

protocol Digraph {
  init(v: Int)
  func addEdge(v: Int, w: Int)
  func adjacentTo(v: Int) -> Set<Int>
  func V() -> Int                       // number of vertices
  func E() -> Int                       // number of edges
  func reverse()
  func toString() -> String
}

extension Digraph {
  func toString() -> String {
    var str = ""
    for v in 0 ..< self.V() {
      for j in self.adjacentTo(v: v) {
        str = str + "\(v) -> \(j)\n"
      }
    }
    return str
  }
}
