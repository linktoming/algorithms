//
//  HamiltonPath.swift
//  Algorithms
//
//  Created by Mingming Wang on 23/08/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

class HamiltonPath {
  private var marked: [Bool]
  private var count = 0

  init(graph: Graph) {
    marked = [Bool](repeating: false, count: graph.V())
    for i in 0 ..< graph.V() {
      dfs(graph, i, 1)
    }
  }

  private func dfs(_ graph: Graph, _ source: Int, _ depth: Int) {
    marked[source] = true
    if depth == graph.V() { count = count + 1}
    for j in graph.adjacentTo(v: source) {
      if (marked[j] == false) {
        dfs(graph, j, depth + 1)
      }
    }
    marked[source] = false
  }
}
