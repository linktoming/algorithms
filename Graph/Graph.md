# Graph
## Graph Terminology
- Graph: set of vertices connected pairwise by edges
- Path: sequence of vertices connected by edges
- Cycle: path whose first and last vertices are the same
- Connected: two vertices are connected if there is a path between them
- Degree: number of edges from the vertex
- Self-loop
- Parallel Edges
- Simple Graph: graph without self-loop and parallel edges
- connected components

## Graph Representation
- drawing (itunitive but can be misleading)
- set of edges
- adjacency-matrix
- adjacency-lists (most practically used)
    - *Algorithms based on iterating over vertices adjacent to v.* 
    - *Real-world graphs tend to be sparse*

We usually use 0 to V-1 to represent vertices. If the key are not numbers, we can use symbel table to conver them into numbers.

### Time and Space Complexity
|representation|space|add edge|edge between v and w?|iterate over vertices adjacent to v?|
| --- | --- | --- | --- | --- |
|set of edegs|E|1|E|E|
|adjacency matrix|V^2|1*|1|V|
|adjacency lists|V+E|1|degree(V)|degree(V)|

**disallows parallel edeges*

## Graph Problems
- Path: Is there a path between s and t ?
- Shortest path: What is the shortest path between s and t ?
- Cycle: Is there a cycle in the graph? DFS will solve it.
- Euler tour: Is there a path that uses each edge exactly once? Solvable.
  - Eulerian Path is a path in graph that visits every edge exactly once. 
  - Eulerian Circuit is an Eulerian Path which starts and ends on the same vertex.
  - A graph is called Eulerian if it has an Eulerian Cycle and called Semi-Eulerian if it has an Eulerian Path.
  - A connected graph is Eulerian if all vertices have even degree.
  - [Eulerian path and circuit for undirected graph](http://www.geeksforgeeks.org/eulerian-path-and-circuit/)
  - [Fleury’s algorithm to find a Euler path or ciruit](https://www.math.ku.edu/~jmartin/courses/math105-F11/Lectures/chapter5-part2.pdf)
    - Make sure the graph has either 0 or 2 odd vertices.
    - If there are 0 odd vertices, start anywhere. If there are 2 odd vertices, start at one of them.
    - Follow edges one at a time. If you have a choice between a bridge and a non-bridge, always choose the non-bridge.
    - Stop when you run out of edges.
    - [local copy](./Euler Paths and Euler Circuits.pdf)
    - Although the algorithm is simple but it's not offten used since check whether a edge is a bridge is not trivial.
- Hamilton tour: Is there a cycle that uses each vertex exactly once. Classic NP-Complete problem.
- Connectivity: Is there a way to connect all of the vertices? DFS
- MST: What is the best way to connect all of the vertices?
- Biconnectivity: Is there a vertex whose removal disconnects the graph?
- Planarity: Can you draw the graph in the plane with no crossing edges? 
  Linear-time DFS-based planarity algorithm discovered by Tarjan in 1970s (too complicated for most practitioners)
- Graph isomorphism: Are two graphs identical except for vertex names? 
  Do two adjacency lists represent the same graph? It's a long standing open problem that no one knows the solution.
- Bipartite: A bipartite graph is a graph whose vertices we can divide into two sets such that 
  all edges connect a vertex in one set with a vertex in the other set. DFS will solve it.

## Depth-first Search
- Goal. Systematically search through a graph. 
- Idea. Mimic maze exploration.
- Algorithm for DFS (to visit a vertex v)
  - Mark v as visited.
  - Recursively visit all unmarked vertices w adjacent to v.
- Typical applications
  - Find all vertices connected to a given source vertex. 
  - Find a path between two vertices.
- Performance: DFS marks all vertices connected to s in time proportional to the sum of their degrees.
  - [correctness] 
    - If w marked, then w connected to s (why?)
    - If w connected to s, then w marked. (if w unmarked, then consider last edge on a path from s 
    to w that goes from a marked vertex to an unmarked one).
  - [running time]
    - Each vertex connected to s is visited once.
- Performance: After DFS, can find vertices connected to s in constant time and can find a path to s 
  (if one exists) in time proportional to its length.

## Breadth-first search
- Depth-first search: put unvisited vertices on a stack. 
- Breadth-first search: put unvisited vertices on a queue.
- Algorithm for BFS (from source vertex s)
  - Put s onto a FIFO queue, and mark s as visited. 
  - Repeat until the queue is empty:
    - remove the least recently added vertex v
    - add each of v's unvisited neighbors to the queue, and mark them as visited.
- BFS examines vertices in increasing distance from s.
- Application: 
  - Shortest path: find path from s to t that uses fewest number of edges.
- Performance: BFS computes shortest paths (fewest number of edges) from s to all other vertices in a graph in time proportional to E + V.
  - [correctness] Queue always consists of zero or more vertices of distance k from s, followed by zero or more vertices of distance k + 1.
  - [running time] Each vertex connected to s is visited once.

## Connectivity Queries
Vertices v and w are connected if there is a path between them.
Preprocess graph to answer queries of the form is v connected to w? in constant time.
A connected component is a maximal set of connected vertices.

## Digraph
Digraph: set of vertices connected pairwise by directed edges.
- directed path
- directed cirle
- indegree, outdegree
### Digraph Problems
- Path: is there a directed path from s to t ?
- Shortest path: what is the shortest directed path from s to t ?
- Topological sort: can you draw a digraph so that all edges point upwards? 
- Strong connectivity: is there a directed path between all pairs of vertices? 
- Transitive closure: for which vertices v and w is there a path from v to w ? 
- PageRank: what is the importance of a web page?
## Weighted Graph

## Weighted Digraph