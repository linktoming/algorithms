//
//  AdjacencyListGraph.swift
//  Algorithms
//
//  Created by Mingming Wang on 24/08/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

struct AdjacencyListGraph:Graph {
  private let vertexCount: Int
  private var edgeCount: Int
  var adjacencyList: [Set<Int>]
  init(V: Int) {
    vertexCount = V
    edgeCount = 0
    adjacencyList = [Set<Int>](repeating: Set<Int>(), count: V)
  }

  mutating func addEdge(v: Int, w: Int) {
    adjacencyList[v].insert(w)
    adjacencyList[w].insert(v)
    edgeCount = edgeCount + 1
  }

  func adjacentTo(v: Int) -> Set<Int> {
    return adjacencyList[v]
  }

  func V() -> Int {
    return vertexCount
  }

  func E() -> Int {
    return edgeCount
  }
}
