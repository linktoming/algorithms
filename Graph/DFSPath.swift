//
//  DFSPath.swift
//  Algorithms
//
//  Created by Mingming Wang on 10/08/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

class DFSPath {
  private var marked = [Bool]()
  private var edgeTo = [Int]()
  private let source: Int

  init(graph: Graph, source: Int) {
    self.source = source
    for i in 0 ..< graph.V() {
      marked.append(false)
      edgeTo[i] = i
    }
    dfs(graph: graph, source: source)
  }

  func hasPathTo(v: Int) -> Bool {
    return marked[v]
  }

  func pathTo(v: Int) -> [Int]? {
    if hasPathTo(v: v) {
      let stack = LinkedListStack<Int>()
      var current = v
      while current != source {
        stack.push(v)
        current = edgeTo[current]
      }
      stack.push(source)
      var result = [Int]()
      for i in stack {
        result.append(i)
      }
      return result
    } else {
      return nil
    }
  }

  private func dfs(graph: Graph, source: Int) {
    marked[source] = true
    for v in graph.adjacentTo(v: source) {
      if marked[v] == false {
        dfs(graph: graph, source: v)
        edgeTo[v] = source
      }
    }
  }
}
