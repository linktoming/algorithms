//
//  BFSPath.swift
//  Algorithms
//
//  Created by Mingming Wang on 18/08/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

class BFSPath {
  private var marked = [Bool]()
  private var edgeTo = [Int?]()
  private let source: Int

  init(graph: Graph, source: Int) {
    marked = [Bool](repeating: false, count: graph.V())
    edgeTo = [Int?](repeating: nil, count: graph.V())
    self.source = source
  }

  private func bfs(graph: Graph, source: Int) {
    let queue = LinkedListQueue<Int>()
    marked[source] = true
    queue.enqueue(source)
    while !queue.isEmpty() {
      let v: Int! = queue.dequeue()
      for j in graph.adjacentTo(v: v) {
        if marked[j] == false {
          marked[j] = true
          edgeTo[j] = v
          queue.enqueue(j)
        }
      }
    }
  }
}
