//
//  Graph.swift
//  
//
//  Created by Mingming Wang on 03/07/2016.
//
//

protocol Graph {
  init(V: Int)                          // create an empty graph with V vertices
  mutating func addEdge(v: Int, w: Int) // add an edge v-w
  func adjacentTo(v: Int) -> Set<Int>   // vertices adjacent to v
  func V() -> Int                       // number of vertices
  func E() -> Int                       // number of edges
  func toString() -> String             // string representation of Graph
}

extension Graph {
  func degree(v: Int) -> Int {
    var degree = 0;
    for _ in self.adjacentTo(v:v) {
      degree = degree + 1
    }
    return degree
  }

  func maxDegree() -> Int {
    var max = 0
    for v in 0..<self.V() {
      let currentDegree = self.degree(v:v)
      max = currentDegree > max ? currentDegree : max
    }
    return max
  }

  func averageDegree() -> Double {
    return 2.0 * Double(self.E()) / Double(self.V())
  }

  func numberOfSelfLoops() -> Int {
    var count = 0
    for v in 0..<self.V() {
      for w in self.adjacentTo(v:v) {
        if v == w {
          count = count + 1
        }
      }
    }

    return count/2
  }

  func printGraph() {
    print(toString())
  }

  func toString() -> String {
    var str = ""
    for i in 0 ..< V() {
      for j in adjacentTo(v: i) {
        str = str + "\(i) - \(j)\n"
      }
    }
    return str
  }
}
