Algorithms
================

## Common Data Structure
1. Array (1 dimension, 2 dimension)
2. Linked List (Singly-lined doubly-linked, circular)
3. String related
4. Hashtable
5. Binary Tree (recursive and non-recursive Traversal,level wise)
6. BST
7. Prefix tree, suffix tree
8. Bit map
9. Simple graph (Graph questions are rare)
10. Dynamic Programming, Divide and Conquer, Recursion, Sorting & Lookup

## Classic Topics on Alorithms
1. Sorting and Searching
2. Graph

## Notes on Swift
- to define the interfaca of a data structure, it's convenient to use class.
- to define the interface of an algorithm, we can use protocol.

## Learning Material & Reference
### Online Course 
- [Algorithms, Part I](https://www.coursera.org/learn/introduction-to-algorithms)
- [Algorithms, Part II](https://www.coursera.org/learn/java-data-structures-algorithms-2)

### Books
- [Algorithms, 4th Edition](http://algs4.cs.princeton.edu/home/) & [Algorithms in C](http://www.cs.princeton.edu/~rs/)
- [Introduction to Algorithms, Third Edition](https://mitpress.mit.edu/books/introduction-algorithms)
- [The Algorithm Design Manual, 2nd Edition](http://www.algorist.com)

### Other Repos
- [Swift Algorithm Club](https://github.com/raywenderlich/swift-algorithm-club)
- [LeetCode Swift](https://github.com/soapyigu/LeetCode_Swift)
- [waynewbishop/SwiftStructures](https://github.com/waynewbishop/SwiftStructures)
- [EvgenyKarkan/EKAlgorithms](https://github.com/EvgenyKarkan/EKAlgorithms)



