//
//  LSDRadixSort.swift
//  Algorithms
//
//  Created by Mingming Wang on 20/08/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//
class LSDRadixSort {
  static func sort(array: inout [String], length: Int) {
    let radix = 256
    var aux = [String?](repeating: nil, count: array.count)

    // Key-indexed Counting for each character
    for i in (0 ... length - 1).reversed() {
      var count = [Int](repeating: 0, count: radix + 1)
      for str in array {
        let index = unicodeForString(string: str, atIndex: i) + 1
        assert(index <= radix)
        count[index] = count[index] + 1
      }
      for i in 1 ... radix {
        count[i] = count[i] + count[i - 1]
      }
      for str in array {
        let unicode = unicodeForString(string: str, atIndex: i)
        aux[count[unicode]] = str
        count[unicode] = count[unicode] + 1
      }
      for i in 0 ..< array.count {
        array[i] = aux[i]!
      }
    }
  }

  static private func stringAtIndex(string: String, index: Int) -> String {
    return String(string[string.index(string.startIndex, offsetBy: index)])
  }

  static private func unicodeForString(string: String, atIndex index: Int) -> Int {
    let str = stringAtIndex(string: string, index: index)
    return Int(str.unicodeScalars.first!.value)
  }
}
