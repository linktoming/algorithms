//
//  KeyIndexedCounting.swift
//  Algorithms
//
//  Created by Mingming Wang on 20/08/2016.
//  Copyright © 2016 Mingming Wang. All rights reserved.
//

class KeyIndexedCounting {
  // assumming array contains integers ranging from 0 to r - 1
  static func sort(array:inout [Int], r: Int) {
    var count = [Int](repeating: 0, count: r + 1)

    // index counting and put the count for i into count[i+1]
    // to make the code simpler
    for i in array {
      count[i + 1] = count[i + 1] + 1
    }
    // count the cumulates, count for items equal or smaller than i will
    // be put in count[i + 1] which is the start position for i + 1
    // which will be
    for i in 1 ... r {
      count[i] = count[i] + count[i - 1]
    }
    // put item in position in auxiliary array
    var aux = [Int](repeating: 0, count: array.count)
    for i in array {
      aux[count[i]] = i
      count[i] = count[i] + 1
    }
    // copy item from aux to array
    for i in 0 ..< aux.count {
      array[i] = aux[i]
    }
  }
}
