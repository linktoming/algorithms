# String
## String Implementation
String is sequence of characters (char).
- in C, the char data type is typically an 8-bit integer. 
  - supports 7-bit ASCII.
  - can represent only 256 characters.
- in Java, the char data type is a 16-bit unsigned integer.
  - supports original 16-bit Unicode.
  - supports 21-bit Unicode 3.0 (awkwardly).

### Java String Implementation
- String:sequence of characters (immutable).
- StringBuilder: sequence of characters (mutable). Underlying implementation is resizing char[] array and length.

![java string](./images/java_string.png)

### Alphabet and Radix
- Digital key: sequence of digits over fixed alphabet. 604 CHAPTER 6 ! Strings
- Radix: number of digits R in alphabet.

## Key-indexed Counting 
- Assumption: keys are integers between 0 and R - 1. 
- Implication: can use key as an array index.
- Goal: Sort an array a[] of N integers between 0 and R - 1. 
  - Count frequencies of each letter using key as index
  - Compute frequency cumulates which specify destinations. 
  - Access cumulates using key as index to move items. 
  - Copy back into original array.
- Proposition: Key-indexed counting uses ~ 11 N + 4 R array accesses to sort 
  N items whose keys are integers between 0 and R - 1. 
- Proposition. Key-indexed counting uses extra space proportional to N + R.
- Key-indexed Counting is stable.
### Least-significant-digit-first (LSD) String (Radix) Sort
- Consider characters from right to left.
- Stably sort using dth character as the key (using key-indexed counting).

LSD is stable.
